<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin\Product;
use App\Admin\Category;
use App\Admin\BannerImage;
use DB;
use App\Helpers\Helpers;

class HomeController extends Controller
{
    Public function getProductslist()
    {
         $data = array(
            'categories' => Category::with('children')->get(),
            'parent_categories' => Category::where('parent_id','=',NULL)->get(),
            'banner_images ' => BannerImage::all(),
        );

        return $data;
    }


    /**
     * HomePage Index
     *
     * @return mixed
     */
    public function index()
    {
        $categories=Category::with('children')->get();
        $parent_categories=Category::where('parent_id','=',NULL)->get();
        $banner_images=BannerImage::all();
        $categoryWiseProduct = $this->getProductByParentCategory();
        return view('home.product.homepage')->with(compact('categories','parent_categories','banner_images','categoryWiseProduct'));
    }


    /**
     * Get Product List
     *
     * @return mixed
     */
    public function getProductList()
    {
        return view('home.product.product_list')->with($this->getProductslist());
    }


    /**
     * Get single Product
     *
     * @param $id - Product id
     * @return mixed
     */
    public function singleProduct($slug)
    {
        $productDetail=$this->getProductDetailBySlug($slug);
        $id = $productDetail->product_id;
        $product= Product::where('slug',$slug)->first();
        return view('home.product.product_singleproduct')
        ->with('product' , $product);
    }


    /**
     * Product Quick View Ajax Response
     *
     * @param $id - Product Id
     * @return mixed
     */
    public function quickView($id)
    {
        $product = $this->service->getById($id);
        $productDetail = [
            'name' => $product->name,
            'description' => $product->meta_description,
            'price' => $product->price,
            'image' => $product->image,
            'product_id' => $id
        ];
        return response()->json($productDetail);
    }


    public function getProductByParentCategory()
    {
        $categoriesWiseProduct = DB::select("SELECT *
            FROM categories
            WHERE 
            parent_id is NULL");
        
        foreach($categoriesWiseProduct as $key=>$value)
        {
            $products = DB::select("SELECT products.*,categories.parent_id,categories.id
                FROM products
                INNER JOIN 
                products_category ON products.`id` = products_category.`product_id`
                INNER JOIN
                categories ON categories.`id` = products_category.`category_id`
                WHERE
                categories.parent_id=$value->id");
            $categoriesWiseProduct[$key]->products = $products;
        }
        return $categoriesWiseProduct;
    }


    public function getProductDetailBySlug($slug)
    {
        $result= Product::where('slug','=',$slug)->first();
        return $result;
    }

    /**
     * Category wise Product
     *
     * @param $categorySlug
     * @param $subCategorySlug
     * @return View
     */
    public function categoryProduct($categorySlug , $subCategorySlug)
    {  
        // $category = $this->cService->getCategoryByCategorySlug($subCategorySlug);
        $category = Category::where('slug',$subCategorySlug)->first();
        $parentCategory = Category::where('id',$category->parent_id)->first();
        $parentname =  $parentCategory->name;
        $categoryname =  $category->name;
        $childCategory= Category::where('parent_id',$category->parent_id)->first();
        // $childCategory = $this->cService->getChidCategoryByParentId($category->parent_id);
        $products = Category::where('slug',$subCategorySlug)->with('products')->get();
        // dd($products);
        return view('home.product.categorywise_productlist')->with(compact('products','categoryname','parentname','childCategory'));
    }

    /**
     * Show Contact view
     * @param $request
     * @return mixed
     */
    public function addEnquiry(Request $request)
    {   
        $data = $request->all();

        $receiverAddress = 'khemrro67@gmail.com';
        // Mail::send(new ProductEnquiry($data));
        Mail::to($receiverAddress)->send(new ProductEnquiry($data));
        $request->session()->flash('success', 'Your Message Sucessfully Send to the Admin');
       return redirect()->back();
        // Mail::send(new ProductEnquiry($data));
    }
}
