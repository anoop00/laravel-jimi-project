<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.dashboard');
    }


    public function showChangePass()
    {
        return view('admin.chagepass.changepass');
    }



    public function postChangePass(Request $request)
    {
      // Validate the form data
      $this->validate($request, [
            'old_password'         => 'required',
            'new_password'         => 'required',
            'confirm_password' => 'required|same:new_password'
      ]);
        // dd($request->all());
      $id = $request->id;
      $password = $request->new_password;
      $newpassword = Hash::make($password);
      Admin::find($id)->update(['password' => $newpassword]);
      $request->session()->flash('success', 'Your Password Change Successfully!');
      return redirect()->route('admin.dashboard');


    }
}
