<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Admin\Setting;
use App\Http\Requests\Admin\SettingRequest;
use App\Http\Controllers\Controller;
use DB;

class SettingController extends Controller
{
    /**
     * SettingEloquentRepository constructor.
     * @param Setting $setting
     */
    public function __construct(Setting $setting)
    {
        $this->model = $setting;
        $this->middleware('auth:admin');

    }

    /**
     * Index View of Setting
     *
     * @return mixed
     */
    public function index()
    {

        return view('admin.setting.admin_table_setting', ['settings' => $this->getSettingByCodeAndKey('config','config_name')]);
    }

    /**
     * Edit Form Data
     *
     * @param $code
     * @return mixed
     */
    public function edit($code)
    {
        $setting = $this->getSettingByCode($code);
        // $orderStatus = Setting::all();
        return view('admin.setting.admin_edit_setting')->with(compact('setting','orderStatus','information'));
    }


    /**
     * Update Setting Data
     *
     * @param SettingRequest $request
     * @param $code
     * @return mixed
     */
    public function update(SettingRequest $request, $code)
    {
        $data = $request->except('_method','_token');
        $this->updateSettingByCode($code, $data);
        $request->session()->flash('success', 'Setting Changes Applied Successfully!');
        return redirect()->route('setting.index');
    }

    public function updateSettingByCode($code,$data)
    {
        $this->model->where('code','=',$code)->delete();
        foreach($data as $key=>$value) {
            if (substr($key, 0, strlen($code)) == $code) {
                if (!is_array($value)) {
                    DB::table('settings')->insert(['code' => $code, 'key' => $key, 'value' => $value]);
                } else {
                    DB::table('settings')->insert(['code' => $code, 'key' => $key, 'value' => json_encode($value),'serialized' =>1]);
                }
            }
        }
        return true;
    }


    /**
     * Get Setting By Code
     *
     * @param $code
     * @return array
     */
    public function getSettingByCode($code)
    {
        $settingData = [];
        $data = $this->model->where('code','=',$code)->get();
        foreach($data as $d){
            if(!$d['serialized']){
                $settingData[$d['key']] = $d->value;
            }
            else{
                $settingData[$d['key']] = json_decode($d->value,true);
            }
        }
        return $settingData;

    }

    /**
     * Get Settings By Code And Key
     *
     * @param $code
     * @param $key
     * @return mixed
     */
    public function getSettingByCodeAndKey($code,$key)
    {
       return $this->model->where('code','=',$code)->where('key','=',$key)->get();

    }
}
