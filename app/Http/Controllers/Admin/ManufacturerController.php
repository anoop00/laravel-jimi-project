<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ManufacturerRequest;
use App\Admin\Manufacturer;

class ManufacturerController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Index View of Manufacturer
     *
     * @return mixed
     */
    public function index()
    {
        $manufacturers=Manufacturer::all();
        return view('admin.manufacturer.admin_table_manufacturer')->with(compact('manufacturers'));
    }

    /**
     * Create Manufacturer
     *
     * @return mixed
     */
    public function create()
    {
        return view('admin.manufacturer.admin_add_manufacturer');
    }

    /**
     * Update Manufacturer
     *
     * @param ManufacturerRequest $request
     * @return mixed
     */
    public function store(Request $request)
    {
        $data = $request->all();
        Manufacturer::create($data);
        $request->session()->flash('success', 'Manufacturer Added Successfully!');
        return redirect()->route('manufacturer.index');
    }

    /**
     * Edit Form Data
     *
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        // $categories= Category::all();
        $manufacturer=Manufacturer::find($id);
        return view('admin.manufacturer.admin_edit_manufacturer')->with(compact('manufacturer'));
    }


    /**
     * Update Manufacturer Data
     *
     * @param ManufacturerRequest $request
     * @param $id
     * @return mixed
     */
    public function update(Request $request, $id)
    {
        // dd($data);
        $data = $request->all();
        Manufacturer::findOrFail($id)->update($data);
        $request->session()->flash('success', 'Manufacturer Changes Applied Successfully!');
        return redirect()->route('manufacturer.index');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
            Manufacturer::findOrFail($id)->delete();
            return 'deleted';
    }

}
