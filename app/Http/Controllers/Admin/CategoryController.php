<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CategoryRequest;
use App\Admin\Category;

class CategoryController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Category Create
     *
     * @return View
     */
    public function create()
    {
        $categories = Category::all();
        return view('admin.category.admin_add_category', compact('categories'));
    }


    /**
     * Add Category index
     *
     * @return View
     */

    public function index()
    {
        $categories= Category::all();
        return view('admin.category.admin_table_category')->with(compact('categories'));


    }

    /**
     * Edit Form Data
     *
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $categories= Category::all();
        $category=Category::find($id);
        return view('admin.category.admin_edit_category')->with(compact('categories','category'));

    }

    /**
     * Update Category Data
     *
     * @param CategoryRequest $request
     * @param $id
     * @return mixed
     */
    public function update(CategoryRequest $request, $id)
    {
        $data = $request->all();
        if (empty($data['top'])): $data['top'] = 0;
        else: $data['top'] = 1; endif;
        Category::findOrFail($id)->update($data);
        $request->session()->flash('success', 'Category Changes Applied Successfully!');
        return redirect()->route('category.index');
    }

    /**
     *
     * @param CategoryRequest $request
     */
    public function store(CategoryRequest $request)
    {
        // dd($request->all());
        $data = $request->all();
        if (empty($data['top'])): $data['top'] = 0;
        else: $data['top'] = 1; endif;
        Category::create($data);
        $request->session()->flash('success', 'Category Added Successfully!');
        return redirect()->route('category.index');
    }

    /**
     * Delete form Data
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        Category::findOrFail($id)->delete();
        return 'deleted';
    }

    /**
     * Delete form Data
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        return view('admin.category.admin_show_category', [
            'category' => $this->service->getById($id),
            'categories' => $this->service->getAll()
        ]);
    }
}
