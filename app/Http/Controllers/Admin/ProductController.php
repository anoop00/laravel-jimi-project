<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin\Product;
use App\Admin\Manufacturer;
use App\Http\Requests\Admin\ProductRequest;
use App\Admin\Category;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    
    public function getProductRelatedModelData()
    {
        $categories = Category::with('children')->get();
        $products=Product::all();
        $manufacturers = Manufacturer::all();
        return $data = array(
            'categories' => $categories,
            'manufacturers' => $manufacturers,
            'products' => $products
        );


    }

    public function index()
    {
        return view('admin.product.admin_table_product')->with($this->getProductRelatedModelData());
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.product.admin_add_product')->with($this->getProductRelatedModelData());
    }

    /**
     * Update product
     *
     * @param ProductRequest $request
     * @return mixed
     */
    public function store(ProductRequest $request)
    {
        $data = $request->all();
        $product=Product::create($data);
        $product->category()->attach($request->category_id);
        $request->session()->flash('success', 'Product Added Successfully!');
        return redirect()->route('product.index');
    }

    /**
     * Edit Form Data
     *
     * @param $id
     * @return mixed
     */
    public function edit($id)
    {
        $product=Product::with('category')->findOrFail($id);
        return view('admin.product.admin_edit_product')
            ->with($this->getProductRelatedModelData())
            ->with(array(
                'product' => $product
            ));

    }

    // public function show($id)
    // {
    //     $productAttributes = $this->service->getProductWithAttributes($id);
    //     $productDiscounts = $this->service->getProductDiscountsByProductId($id);
    //     $productImages = $this->service->getProductImagesByProductId($id);
    //     return view('admin.product.admin_show_product')
    //         ->with($this->getProductRelatedModelData())
    //         ->with(array(
    //             'product' => $this->service->getById($id),
    //             'productAttributes' => $productAttributes,
    //             'productImages' => $productImages,
    //             'productDiscounts' => $productDiscounts
    //         ));

    // }


    /**
     * Update product Data
     *
     * @param ProductRequest $request
     * @param $id
     * @return mixed
     */
    public function update(ProductRequest $request, $id)
    {
        // dd($request->category_id);
        $data=$request->all();
        Product::findOrFail($id)->update($data);
        $product=Product::findOrFail($id);
        $product->category()->sync($request->category_id);
        $request->session()->flash('success', 'Product Changes Applied Successfully!');
        return redirect()->route('product.index');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        // dd('sdfjkdsfk');
        Product::findOrFail($id)->delete();
        return 'deleted';;
    }
}
