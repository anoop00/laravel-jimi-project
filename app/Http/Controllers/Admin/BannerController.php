<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use AppRepo\Services\BannerService;
use App\Http\Requests\Admin\BannerRequest;
use AppRepo\Services\BannerImageService;

class BannerController extends Controller
{
	/**
     * Banner Service
     *
     * @var object $service
     */
    private $service;

     /**
     * BannerImage Service 
     *
     * @var object $service
     */
    private $bimgService;

    /**
     * BannerController constructor.
     * @param BannerService $service
     */
    public function __construct(BannerService $service, BannerImageService $bannerimageservice)
    {
        $this->service = $service;
        $this->bimgService = $bannerimageservice;
        $this->middleware('auth:admin');
    }

    public function create()
    {
        return view('admin.banner.admin_add_banner');
    }


    /**
     * Add Banner index
     *
     * @return View
     */

    public function index(){
        return view('admin.banner.admin_table_banner',['banner'=> $this->service->getAll()]);
       

    }

    /**
     * Edit Form Data
     *
     * @param $id
     * @return mixed
     */
    public function edit($id){
        return view('admin.banner.admin_edit_banner',
                        ['banner'=>$this->service->getById($id)],
                        ['bannerimage'=>$this->service->getBannerImagesByBannerId($id)]
            );
        
    }

    /**
     * Update Banner Data
     *
     * @param BannerRequest $request
     * @param $id
     * @return mixed
     */
    public function update(BannerRequest $request, $id)
    {
        // dd($request->all());
        $this->service->update($id, $request->all());
        $request->session()->flash('success', 'Banner Changes Applied Successfully!');
        return redirect()->route('banner.index');
    }

    /**
     * 
     * @param BannerRequest $request
     */
    public function store(BannerRequest $request) 
    {
        $this->service->store($request->all());
        $request->session()->flash('success', 'Banner Added Successfully!');
        return redirect()->route('banner.index');
    }

     /**
     * Delete form Data
     * @param $id
     * @return mixed
     */
     public function destroy($id)
    {
        $this->service->destroy($id);
        return 'deleted';
    }
}
