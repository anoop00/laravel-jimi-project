<?php
namespace App\Http\ViewComposers;

use Illuminate\View\View;
use Session;
use Auth;
use App\Admin\Setting;
use App\Admin\Category;
use App\Http\Controllers\Controller;


class HeaderComposer
{

	/**
	 * HeaderComposer constructor.
	 * @param WishListService $service
	 */
    public function __construct(Setting $setting)
    {
    	$this->model = $setting;
    }

	/**
	 * @param View $view
	 */
	public function compose(View $view)
	{
		$code='config';
		$setting= $this->getSettingByCode($code);
		$parent_categories= Category::with('children')->get();
	   	$view->with('setting', $setting)
	   		->with('parent_categories', $parent_categories);
	}


	/**
     * Get Setting By Code
     *
     * @param $code
     * @return array
     */
    public function getSettingByCode($code)
    {
        $settingData = [];
        $data = $this->model->where('code','=',$code)->get();
        foreach($data as $d){
            if(!$d['serialized']){
                $settingData[$d['key']] = $d->value;
            }
            else{
                $settingData[$d['key']] = json_decode($d->value,true);
            }
        }
        return $settingData;

    }

    
}