<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->composerview();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    private function composerview()
    {
        view()->composer(
            'home.includes.header', 'App\Http\ViewComposers\HeaderComposer'
        );
        view()->composer(
            'home.includes.footer', 'App\Http\ViewComposers\HeaderComposer'
        );
    }
}
