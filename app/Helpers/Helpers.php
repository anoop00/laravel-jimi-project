<?php
/**
 * Created by PhpStorm.
 * User: khem
 * Date: 9/20/16
 * Time: 11:48 AM
 */

namespace App\Helpers;

/**
 * Class Helper
 * @package App\Helpers
 * @author Khem Raj Regmi <khemrr067@gmail.com>
 */
class Helpers
{

    /**
     * Array of Ajax Routes
     *
     * @var array AjaxRoutes
     */
    // public static $ajaxRoutes=['cities','areas','states','get_make_store_admin','make_or_remove_user_as_store_admin','getProducts','products'];

    /**
     * Array Search in multidimensional Array
     *
     * @param $needle
     * @param $haystack
     * @param bool $strict
     * @return bool
     */
    public static function in_array_r($needle, $haystack, $strict = false)
    {
        foreach ($haystack as $item) {
            if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && self::in_array_r($needle, $item,
                        $strict))
            ) {
                return true;
            }
        }

        return false;
    }

   public static function getBySlug()
   {
        dd('you reach hear');
   }

    
}