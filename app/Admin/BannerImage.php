<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;

class BannerImage extends Model
{
    protected $fillable = [
        'banner_image_id',
        'banner_id',
        'language_id',
        'title',
        'link',
        'image',
        'active_on',
        'expire_on',
        'sort_order'
    ];
}
