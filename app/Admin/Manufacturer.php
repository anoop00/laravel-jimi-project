<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Manufacturer
 * @package App\Admin
 */
class manufacturer extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    
   


    
    /**
     * Fillable Data
     *
     * @var array
     */
    protected $fillable = ['name','image'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    

}
