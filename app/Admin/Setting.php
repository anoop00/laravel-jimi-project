<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;
use DB;

class Setting extends Model
{
    protected $fillable = [
        'store_id',
        'code',
        'key',
        'value',
        'serialized'
    ];


    public static function getSettingByCode($code)
    {
        $settingData = [];
        $data = Setting::where('code','=',$code)->get();
        foreach($data as $d){
            if(!$d['serialized']){
                $settingData[$d['key']] = $d->value;
            }
            else{
                $settingData[$d['key']] = json_decode($d->value,true);
            }
        }
        return $settingData;

    }
}
