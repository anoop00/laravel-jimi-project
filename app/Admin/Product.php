<?php

namespace App\Admin;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Cviebrock\EloquentSluggable\Sluggable;

class Product extends Model
{
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    
    use Sluggable;
    /**
     * @var string
     */
    protected $guarded = ['manufacturer_id'];

    /**
     * Primary key
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'slug',
        'description',
        'tag',
        'model',
        'sku',
        'quantity',
        'image',
        'newarrival',
        'manufacturer_id',
        'price',
        'weight',
        'status',
        'viewed',
        'minimum',
        'meta_title',
        'meta_description',
        'meta_keywords',
    ];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }


     /**
     * @return $this
     */
    public function manufacturer()
    {
        return $this->belongsTo('App\Admin\Manufacturer','manufacturer_id');
    }

   


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function category()
    {
        return $this->belongsToMany(Category::class, 'products_category', 'product_id','category_id')->withTimestamps();
    }

   
}
