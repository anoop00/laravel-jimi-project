<?php

namespace App\Catalog;

use Illuminate\Database\Eloquent\Model;

class Manufacture extends Model
{
    protected $fillable = ['name','image'];
}
