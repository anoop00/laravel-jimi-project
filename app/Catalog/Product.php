<?php

namespace App\Catalog;

// use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'name',
        'slug',
        'description',
        'tag',
        'model',
        'sku',
        'quantity',
        'image',
        'newarrival',
        'manufacturer_id',
        'shipping',
        'price',
        'weight',
        'weight_class_id',
        'length',
        'height',
        'width',
        'status',
        'viewed',
        'minimum',
        'meta_title',
        'meta_description',
        'meta_keywords'
    ];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    /**
     * @return $this
     */
    public function manufacturer()
    {
        return $this->belongsTo('Suvalav\Models\Manufacturer','manufacturer_id');
    }
}
