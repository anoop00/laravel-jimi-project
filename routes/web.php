<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('layouts.app');
// });

Route::get('/faqs', function () {
    return view('home.includes.faqs');
});

Route::get('/contactus', function () {
    return view('home.includes.contactus');
});
Route::get('/aboutus', function () {
    return view('home.includes.aboutus');
});

	Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('login');
	Route::prefix('admin')->group(function() {
		Route::get('/', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
		Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
		Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
		Route::get('/dashboard', 'AdminController@index')->name('admin.dashboard');
		Route::post('/logout', 'Auth\AdminLoginController@logout')->name('logout');
		Route::get('/change-password', 'AdminController@showChangePass')->name('changepassword');
		Route::post('/change-password', 'AdminController@postChangePass')->name('changepassword');

		Route::resource('category', 'Admin\CategoryController');
		Route::resource('banner', 'Admin\BannerController');
		Route::resource('product', 'Admin\ProductController');
		Route::resource('manufacturer', 'Admin\ManufacturerController');
		Route::resource('setting', 'Admin\SettingController');


		// Password reset routes
	    Route::post('/password/email', 'Auth\AdminForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
	    Route::get('/password/reset', 'Auth\AdminForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
	    Route::post('/password/reset', 'Auth\AdminResetPasswordController@reset');
	    Route::get('/password/reset/{token}', 'Auth\AdminResetPasswordController@showResetForm')->name('admin.password.reset');
	});

	Route::get('/', 'Home\HomeController@index')->name(' ');
	Route::get('/products', 'Home\HomeController@getProductList')->name('product.list');
	// Route::get('/quickview/{id}', 'Home\HomeController@quickView')->name('quickview');
	Route::get('/singleproduct/{slug}', 'Home\HomeController@singleProduct')->name('single.product');
	Route::get('/product/{category}/{subcategory}', 'Home\HomeController@categoryProduct')->name('category.product');
	Route::post('enquiry/submit', 'Home\HomeController@addEnquiry')->name('enquiry.submit');
