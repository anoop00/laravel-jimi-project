<?php

use Illuminate\Database\Seeder;
use App\Admin\Category;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->delete();

        Category::insert([
            [	
            	'id'=>1,
            	'parent_id'=>NULL,
		        'name'=>'Badam',
		        'description'=>'This is Badam Category',
		        'image'=>'',
		        'slug'=>'badam',
		        'status'=>'1',
		        'top'=>1
            ],
            [	
            	'id'=>2,
            	'parent_id'=>NULL,
		        'name'=>'Bhatmas',
		        'description'=>'This is Bhatmas Category',
		        'image'=>'',
		        'slug'=>'bhantmas',
		        'status'=>'1',
		        'top'=>1
            ],
            [	
            	'id'=>3,
            	'parent_id'=>NULL,
		        'name'=>'Chana',
		        'description'=>'This is Chana Category',
		        'image'=>'',
		        'slug'=>'chana',
		        'status'=>'1',
		        'top'=>1
            ],
            [	
            	'id'=>4,
            	'parent_id'=>NULL,
		        'name'=>'Corn Seed Foods',
		        'description'=>'This is Corn Seed Foods Category',
		        'image'=>'',
		        'slug'=>'corn-seed',
		        'status'=>'1',
		        'top'=>0
            ],
            [	
            	'id'=>5,
            	'parent_id'=>NULL,
		        'name'=>'Dal Goods',
		        'description'=>'This is Dal Goods Category',
		        'image'=>'',
		        'slug'=>'dal',
		        'status'=>'1',
		        'top'=>0
            ],
            [	
            	'id'=>6,
            	'parent_id'=>NULL,
		        'name'=>'Flour',
		        'description'=>'This is Flour Category',
		        'image'=>'',
		        'slug'=>'flour',
		        'status'=>'1',
		        'top'=>0
            ],
            [	
            	'id'=>7,
            	'parent_id'=>NULL,
		        'name'=>'Oil ',
		        'description'=>'This is Oil  Category',
		        'image'=>'',
		        'slug'=>'oil',
		        'status'=>'1',
		        'top'=>0
            ],
            [	
            	'id'=>8,
            	'parent_id'=>NULL,
		        'name'=>'Lentils ',
		        'description'=>'This is Lentils  Category',
		        'image'=>'',
		        'slug'=>'lentils',
		        'status'=>'1',
		        'top'=>0
            ],
            [	
            	'id'=>9,
            	'parent_id'=>NULL,
		        'name'=>'Rice ',
		        'description'=>'This is Rice  Category',	
		        'image'=>'',
		        'slug'=>'rice',
		        'status'=>'1',
		        'top'=>0
            ]
            ]);
    }
}
