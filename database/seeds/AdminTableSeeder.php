<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->delete();


        DB::table('admins')->insert([
            'name' => 'Khem Raj Regmi',
            'email' => 'khemrr067@gmail.com',
            'password' => bcrypt('password'),
            'job_title'=>'Admin',
            'remember_token'=>'BomCtY638oQxkKKjMp2U5Z117a6Obn7CW3K2hEegMv1ul0jhar...'

        ]);
    }


}
