<?php

use Illuminate\Database\Seeder;
use App\Admin\Setting;

class SettingTableSeeder extends Seeder
{
    
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
          'config_name' => 'Gimmy',
           'config_logo' => '',
           'config_address' => 'kathmandu',
           'config_email' => 'khemrr067@gmail.com',
           'config_telephone' => '98888888',
           'config_fax' => '9888888',
           'config_meta_title' => 'gimmy',
           'config_meta_description' => 'gimmy',
           'config_meta_keyword' => 'gimmy',
           
        ];
        $code = 'config';
        DB::table('settings')->delete();
       foreach($data as $key=>$value) {
            if (substr($key, 0, strlen($code)) == $code) {
                if (!is_array($value)) {
                    DB::table('settings')->insert(['code' => $code, 'key' => $key, 'value' => $value]);
                } else {
                    DB::table('settings')->insert(['code' => $code, 'key' => $key, 'value' => json_encode($value),'serialized' =>1]);
                }
            }
        }
    }
}
