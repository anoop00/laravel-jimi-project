<?php

use Illuminate\Database\Seeder;
use App\Admin\manufacturer;

class ManufacturerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('manufacturers')->delete();

        manufacturer::insert([
            [	
            	'id'=>1,
            	'image'=>NULL,
		        'name'=>'Gimmy'
            ]]);
    }
}
