<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->string('slug', 32);
            $table->text('description');
            $table->text('tag');
            $table->string('model', 64);
            $table->string('sku', 64);
            $table->integer('quantity');
            $table->string('image', 255);
            $table->integer('manufacturer_id')->unsigned();
            $table->tinyInteger('newarrival')->nullable();
            $table->decimal('price', 15, 4);
            $table->decimal('weight', 15, 4);
            $table->tinyInteger('status')->default(0);
            $table->integer('viewed')->nullable();
            $table->integer('minimum')->nullable();
            $table->string('meta_title', 255);
            $table->string('meta_description', 255);
            $table->string('meta_keywords', 255);
            $table->foreign('manufacturer_id')->references('id')->on('manufacturers')->onDelete('cascade');;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
