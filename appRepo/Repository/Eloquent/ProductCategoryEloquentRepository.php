<?php
/**
 * Created by Sublime.
 * User: Khem
 * Date: 9/05/16
 * Time: 10:00 PM
 */

namespace AppRepo\Repository\Eloquent;





use App\Admin\ProductCategory;
use AppRepo\Repository\Eloquent\BaseEloquentRepository;
use AppRepo\Repository\ProductCategoryRepositoryInterface;
/**
 * Class ProductCategoryEloquentRepository
 * @package SuvalavRepo\Repository\Eloquent
 * @author Khem Raj Regmi <khemrr067@gmail.com>
 */

class ProductCategoryEloquentRepository extends BaseEloquentRepository implements ProductCategoryRepositoryInterface
{

    /**
     * ReturnEloquentRepository constructor.
     *  
     * @param ProductCategory $productcategory
     */
    public function __construct(ProductCategory $productcategory)
    {
        $this->model = $productcategory;
    }

    
    
}