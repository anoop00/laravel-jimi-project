<?php
/**
 * Created by PhpStorm.
 * User: drudge
 * Date: 8/26/16
 * Time: 8:25 AM
 */

namespace AppRepo\Repository\Eloquent;

use AppRepo\Repository\Eloquent\BaseEloquentRepository;
use App\Models\Product;
use AppRepo\Repository\ProductRepositoryInterface;
use DB;

class ProductEloquentRepository extends BaseEloquentRepository implements ProductRepositoryInterface
{
    /**
     * ProductEloquentRepository constructor.
     *
     * @param Product $product
     */
    public function __construct(Product $product)
    {
        $this->model = $product;
    }

    /****
     * ============================================
     *
     * Admin/Backend/Dashboard Related Methods starts from here
     *
     * ===========================================
     ****/


    /**
     * Create Product Image
     *
     * @param $data
     * @return static
     */
    public function create($data)
    {
        return $this->model->create($data);
    }

    /**
     * Update Products
     *
     * @param $productId
     * @param $products
     * @return mixed
     */
    public function update($productId, $products)
    {
        return $this->model->where('product_id', '=', $productId)->update($products);
    }


    /**
     * @param $categoryId
     * @param $product
     * @return mixed
     */
    public function storeProductCategory($categoryId, $product)
    {
     return  $product->Category()->sync($categoryId);
    }


    public function storeProductInStores($storeId,$product)
    {
        return $product->Store()->sync($storeId);
    }

    /**
     * Get Store With Product Acc To User
     *
     * @param $user
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getStoreWithProductAccToUser($user)
    {
            $stores = $user->Store()->get();
            foreach($stores as $key=>$value)
            {
                $stores[$key]['products'] = $value->products()->with('stockStatus')->get();
            }
            return $stores;
    }

    /**
     * Get Store  Acc To User
     *
     * @param $user
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function checkUserStoreAccess($user,$id)
    {
            $stores = $user->Store()->where('suv_store_user.store_id',$id)->exists();
            return $stores;
    }

    /**
     * @param $store
     * @return mixed
     */
    public function getProductAccToStore($store)
    {   
        return $store->first()->products()->get();
    }

    /**
     * Stock Deduction of product after order
     * @param $quantity
     * @return mixed
     */
    public function minimizeStock($quantity)
    {
        foreach ($quantity as $qty) 
        {
            $result= $this->model->where('product_id','=',$qty->id)->first();
            $remaining_quantity = $result['quantity'] - $qty->qty;
            $this->model->where('product_id','=',$qty->id)
                    ->update(['quantity' => $remaining_quantity]);
        }
    }



    public function updateStock($orderProduct)
    {
        DB::statement("UPDATE " . $this->model->getTable() . " SET quantity = (quantity - " . (int)$orderProduct->pivot->quantity . ") WHERE product_id = '" . (int)$orderProduct->pivot->product_id . "' AND subtract = '1' ");
        return true;
    }

    /**
     * @param $store
     * @return mixed
     */
    public function getProductViews($store)
    {   
        if(!isset($store))
        {
            return $this->model->where('viewed', '>=', 1)->orderBy('viewed','DESC')->get();
        }
        else
        {   

            return $store->products()->where('viewed', '>=', 1)->orderBy('viewed','DESC')->get();
        }
    }

    /**
     * @param $store 
     * @return mixed
     */
    public function getTotalViews($store)
    {   if(!isset($store))
        {
            return $this->model->sum('viewed');
        }
        else
        {   
            return $store->products()->sum('viewed');
        }
    }

    /**
     * @param $data
     * @param $store
     * @return mixed
     */
    public function getProductInventoryReport($data,$store)
    {
        if(!isset($store))
        {
        if($data['stock_status_id']==0)
              {
               return $this->model->get();
              }
              else
              {
               return $this->model->where('stock_status_id', '=',$data['stock_status_id'])->get();
              }
        }
        else
        {
           if($data['stock_status_id']==0)
              {
               return $store->products()->get();
              }
              else
              {
               return $store->products()->where('stock_status_id', '=',$data['stock_status_id'])->get();
              } 
        }
    }

    

    /****
     * ============================================
     *
     * Admin/Backend/Dashboard Related Methods Ends here
     *
     * ===========================================
     ****/





    /****
     * ============================================
     *
     * Frontend Related Methods starts from here
     *
     * ===========================================
     ****/

    /**
     * @return mixed
     */
    public function productPagination()
    {
        return $this->model->where('status', '=', 1)->paginate(9);
    }


    /**
     * @param $search
     * @return mixed
     */
    public function getBySearch($search)
    {
        return $this->model->where('name', 'LIKE', '%'. $search .'%')->paginate(9);
    }

     /**
     * @param $slug
     * @return mixed
     */
    public function updateViewCount($slug)
    {
        $data= $this->model->where('slug', '=', $slug)->first();
        return $this->model->where('slug','=',$slug)
            ->update(['viewed' => $data['viewed']+1]);
    }

    /**
     * @param $category
     * @return mixed
     */
    public function getProductsByCategory($category)
    {
        return $category->products()->paginate(9);
    }


    

    /****
     * ============================================
     *
     * Frontend Related Methods ends from here
     *
     * ===========================================
     ****/

}