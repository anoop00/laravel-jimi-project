<?php
/**
 * Created by PhpStorm.
 * User: drudge
 * Date: 8/31/16
 * Time: 10:46 AM
 */

namespace AppRepo\Repository\Eloquent;

use App\Models\Manufacturer;
use AppRepo\Repository\ManufacturerRepositoryInterface;


/**
 * Class ManufacturerEloquentRepository
 * @package AppRepo\Repository\Eloquent
 * @author Rajendra Sharma <drudge.rajan@gmail.com>
 */
class ManufacturerEloquentRepository extends BaseEloquentRepository implements ManufacturerRepositoryInterface
{

    /**
     * ManufacturerEloquentRepository constructor.
     * @param Manufacturer $manufacturer
     */
    public function __construct(Manufacturer $manufacturer)
    {
        $this->model = $manufacturer;
    }

     /**
     * @param $manufacturerId
     * @param $manufacuturer
     * @return mixed
     */
    public function storeManufacturerInStores($manufacturerId, $manufacuturer)
    {
     return  $manufacuturer->Store()->sync($manufacturerId);
    }


    /**
     * Get Store With Manufacturer Acc To User
     *
     * @param $user
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getStoreWithManufacturerAccToUser($user)
    {
            $stores = $user->Store()->get();
            foreach($stores as $key=>$value)
            {
                $stores[$key]['manufacturers'] = $value->manufacturers()->get();
            }
            return $stores;
    }
}