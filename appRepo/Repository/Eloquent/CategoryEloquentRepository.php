<?php
/**
 * Created by Sublime.
 * User: khem
 * Date: 8/24/16
 * Time: 12:20 PM
 */

namespace AppRepo\Repository\Eloquent;


use App\Admin\Category;
use AppRepo\Repository\Eloquent\BaseEloquentRepository;
use AppRepo\Repository\CategoryRepositoryInterface;
use Illuminate\Support\Facades\DB;

class CategoryEloquentRepository extends BaseEloquentRepository implements CategoryRepositoryInterface
{

    /**
     * CategoryEloquentRepository constructor.
     *
     * @param Category $category
     */
    public function __construct(Category $category)
    {
        $this->model = $category;
    }

    /**
     * Get Products By Category Id
     *
     * @param $categoryId
     * @return mixed
     */
    public function getProductsByCategoryId($categoryId)
    {
        $category = $this->findById($categoryId);
        return $category->products;
    }

    /**
     * Parent Category
     *
     * @return mixed
     */
    public function parentCategory()
    {
        return $this->model->where('parent_id','=',NULL)->get();
    }


     /**
     * Top Category
     *
     * @return mixed
     */
    public function topCategory()
    {
        return $this->model->with('children')->where('top',1)->get();
    }


    /**
     * Product by Category Slug
     *
     * @return mixed
     */
    public function getCategoryByCategorySlug($slug)
    {
        return $category = $this->findBySlug($slug);

    }

    /**
     * @param $storeId
     * @param $category
     * @return mixed
     */
    public function storeCategoryInStores($storeId,$category)
    {
        return  $category->store()->sync($storeId);
    }
    

    public function getCategorylist($storeId)
    {
        // $data = $this->model->with('allChildren')
        // ->wherePivot('store_id','=',$storeId)
        // ->get();

        // dd($data);

         // $storeCategory = DB::select("SELECT suv_categories.*
         //                    FROM suv_categories
         //                    INNER JOIN 
         //                    suv_store_category ON suv_categories.`category_id` = suv_store_category.`category_id`
         //                    INNER JOIN
         //                    suv_stores ON suv_stores.`store_id` = suv_store_category.`store_id`
         //                    WHERE suv_stores.store_id =$storeId");
         // return $storeCategory;

        $storeCategory = DB::select("SELECT suv_categories.*
        FROM suv_categories
        INNER JOIN 
        suv_store_category ON suv_categories.`category_id` = suv_store_category.`category_id`
        INNER JOIN
        suv_stores ON suv_stores.`store_id` = suv_store_category.`store_id`
        WHERE suv_stores.store_id =$storeId");




        //        dd($storeProducts);
                $store_cat = [];

                foreach ($storeCategory as $storeCat) {
                    if ($storeCat->parent_id == null) {
                        $store_cat[$storeCat->category_id] = [];
                        $store_cat[$storeCat->category_id]['cat']= $storeCat;
                        $store_cat[$storeCat->category_id]['child'] = [];
                    }
                    else{
                        $store_cat[$storeCat->parent_id]['child']['cat'] = $storeCat;
                    }
                }

                return $store_cat;

    }
    
}