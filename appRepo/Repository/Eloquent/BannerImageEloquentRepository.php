<?php
/**
 * Created by Sublime.
 * User: Khem
 * Date: 9/16/16
 * Time: 12:00 PM
 */

namespace AppRepo\Repository\Eloquent;



use App\Admin\BannerImage;
use AppRepo\Repository\BannerImageRepositoryInterface;
/**
 * Class BannerImageEloquentRepository
 * @package AppRepo\Repository\Eloquent
 * @author Khem Raj Regmi <khemrr067@gmail.com>
 */

class BannerImageEloquentRepository extends BaseEloquentRepository implements BannerImageRepositoryInterface
{
    /**
     * BannerImageEloquentRepository constructor.
     *  
     * @param BannerImage $bannerimage
     */
    public function __construct(BannerImage $bannerimage)
    {
        $this->model = $bannerimage;
    }

    /**
     * Store Banner Images
     *
     * @param $bannerImages
     * @param $bannerId
     */
    public function storeBannerImages($bannerImages,$bannerId)
    {
        foreach($bannerImages as $bi)
        {
            $this->model->create([
                  
                  'banner_id' => $bannerId,
                  'title' => ($bi['title'] == "") ? " " : $bi['title'],
                  'link' => ($bi['link'] == "") ? " " : $bi['link'],
                  'image' => ($bi['image'] == "") ? "No Image " : $bi['image'],
                  'sort_order' => ($bi['sort_order'] == 1) ? "No Image " : $bi['sort_order']
            ]);
        }
    }


    /**
     * Update Banner Images
     * 
     * @param $bannerImages
     * @param $bannerId
     */
    public function updateBannerImages($bannerImages,$bannerId)
    {   
        // dd($bannerImages);
        $this->model->where('banner_id','=',$bannerId)->delete();
        foreach($bannerImages as $bi)
        {
            $this->model->create([
                  'banner_id' => $bannerId,
                  'title' => ($bi['title'] == "") ? " " : $bi['title'],
                  'link' => ($bi['link'] == "") ? " " : $bi['link'],
                  'image' => ($bi['image'] == "") ? "No Image " : $bi['image'],
                  'sort_order' => ($bi['sort_order'] == "") ? 1 : $bi['sort_order']
            ]);
        }
    }

    /**
     * Get Banner Images By Banner ID
     * 
     * @param $bannerId
     * @return mixed
     */
    public function getBannerImagesByBannerId($bannerId)
    {
        return $this->model->where('banner_id','=',$bannerId)->get();
    }
    
}