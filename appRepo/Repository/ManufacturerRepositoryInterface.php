<?php
/**
 * Created by PhpStorm.
 * User: drudge
 * Date: 8/31/16
 * Time: 10:48 AM
 */

namespace AppRepo\Repository;


/**
 * Interface ManufacturerRepositoryInterface
 * @package AppRepo\Repository
 * @author Rajendra Sharma <drudge.rajan@gmail.com>
 */
interface ManufacturerRepositoryInterface
{
	/**
     * @param $data
     * @param $manufacuturer
     * @return mixed
     */
    public function storeManufacturerInStores($data,$manufacuturer);


    /**
     * @param $user
     * @return mixed
     */
    public function getStoreWithManufacturerAccToUser($user);
}