<?php
/**
 * Created by PhpStorm.
 * User: Khem
 * Date: 8/24/16
 * Time: 10:52 AM
 */

namespace AppRepo\Repository;

/**
 * Interface CategoryRepositoryInterface
 * 
 * @package AppRepo\Repository
 * @author Khem Raj Regmi <drudge.rajan@gmail.com>
 */
interface CategoryRepositoryInterface
{

    /**
     * Get Products By Category Id
     * 
     * @param $categoryId
     * @return mixed
     */
    public function getProductsByCategoryId($categoryId);


	/**
     * @return mixed
     */
    public function parentCategory();

    /**
     * @return mixed
     */
    public function topCategory();

    /**
     *@param $slug
     * @return mixed
     */
    public function getCategoryByCategorySlug($slug);


    /**
     * @param $data
     * @param $category
     * @return mixed
     */
    public function storeCategoryInStores($data,$category);


    public function getCategorylist($storeId);
    
	
}