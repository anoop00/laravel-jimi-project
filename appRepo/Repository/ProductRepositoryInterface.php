<?php
/**
 * Created by PhpStorm.
 * User: Khemrr
 * Date: 8/26/16
 * Time: 8:24 AM
 */

namespace AppRepo\Repository;


/**
 * Interface ProductRepositoryInterface
 * @package AppRepo\Repository
 * @author Khem <Khemrr067@gmail.com>
 */
interface ProductRepositoryInterface
{


    /****
     * ============================================
     *
     * Admin/Backend/Dashboard Related Methods starts from here
     *
     * ===========================================
     ****/


    /**
     * @param $products
     * @return mixed
     */
    public function create($products);

    /**
     * @param $productId
     * @param $products
     * @return mixed
     */
    public function update($productId,$products);

    /**
     * @param $data
     * @param $product
     * @return mixed
     */
    public function storeProductCategory($data,$product);


    /**
     * @param $data
     * @param $product
     * @return mixed
     */
    public function storeProductInStores($data,$product);
    
    /**
     * @param $user
     * @return mixed
     */
    public function getStoreWithProductAccToUser($user);

    /**
     * @param $store
     * @return mixed
     */
    public function getProductAccToStore($store);


    /**
     * @param $quantity
     * @return mixed
     */
    public function minimizeStock($quantity);

    /**
     * @param $orderProduct
     * @return mixed
     */
    public function updateStock($orderProduct);

    /**
     * @param $store
     * @return mixed
     */
    public function getProductViews($store);

    /**
     * 
     * @param $store
     * @return mixed
     */
    public function getTotalViews($store);

    /**
     * @param $data
     * @param $store
     * @return mixed
     */
    public function getProductInventoryReport($data,$store);

    /**
     * @param $user
     * @param $id
     * @return mixed
     */
    public function checkUserStoreAccess($user,$id);

    

    /****
     * ============================================
     *
     * Admin/Backend/Dashboard Related Methods Ends here
     *
     * ===========================================
     ****/


    /****
     * ============================================
     *
     * Frontend Related Methods starts from here
     *
     * ===========================================
     ****/
    /**
     * @return mixed
     */
    public function productPagination();

    /**
     * @param $search
     * @return mixed
     */
    public function getBySearch($search);

    /**
     * @param $slug
     * @return mixed
     */
    public function updateViewCount($slug);

    /**
     * @param $category
     * @return mixed
     */
    public function getProductsByCategory($category);

    
    /****
     * ============================================
     *
     * Frontend Related Methods ends here
     *
     * ===========================================
     ****/


    


   
    
}