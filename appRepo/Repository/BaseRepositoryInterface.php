<?php

/**
 * Created by Sublime.
 * User: Khem
 * Date: 11/06/16
 * Time: 8:19 PM
 */
namespace AppRepo\Repository;

/**
 * Interface BaseRepositoryInterface
 * @package AppRepo\Repository
 * @author Khem Raj Regmi <khemrr067@gmail.com>
 */
interface BaseRepositoryInterface
{
    /**
     * @param $data
     * @return mixed
     */
    public function create($data);

    /**
     * @param $data
     * @param $id
     * @return mixed
     */
    public function update($id, $data);

    /**
     * @param $id
     * @return mixed
     */
    public function findById($id);

    /**
     * @return mixed
     */
    public function getAll();


    /**
     * @param array $id
     * @return mixed
     */
    public function destroy(array $id);

    /**
     * @param $slug
     * @return mixed
     */
    public function findBySlug($slug);

    /**
     * @return mixed
     */
    public function getAllEnable();
}