<?php

namespace AppRepo\Services;

use AppRepo\Services\BaseService;
use AppRepo\Repository\CategoryRepositoryInterface;
use AppRepo\Repository\ProductCategoryRepositoryInterface;
use App\Admin\ProductCategory;


/**
 * Class CategoryService
 * @package AppRepo\Services
 */
class CategoryService extends BaseService
{
     /**
     * @var ProductCategoryRepositoryInterface
     */
    protected $productCategoryRepo;


	/**
     * CategoryService constructor.
     * @param CategoryRepositoryInterface $repo
     * @param ProductCategoryRepositoryInterface $productCategoryRepositoryInterface
     */
    public function __construct(CategoryRepositoryInterface $repo,
                                ProductCategoryRepositoryInterface $productCategoryRepositoryInterface)
    {
        $this->repo = $repo;
        $this->productCategoryRepo = $productCategoryRepositoryInterface;
    }

    public function storeCategory($data)
    {
        $category = [
          'name'=>$data['name'],
          'description'=>$data['description'],
          'meta_title'=>$data['meta_title'],
          'meta_keyword'=>$data['meta_keyword'],
          'meta_description'=>$data['meta_description'],
          'parent_id'=>$data['parent_id'],
          'image'=>$data['image'],
          'status'=>$data['status'],
          'top'=>$data['top'],
        ];

       $category=$this->store($category);
        if($data['store_id'])
        {
            $this->repo->storeCategoryInStores($data['store_id'],$category);
        }

    }

    public function updateCategory($id,$data)
    {
        $category = [
            'name'=>$data['name'],
            'description'=>$data['description'],
            'meta_title'=>$data['meta_title'],
            'meta_keyword'=>$data['meta_keyword'],
            'meta_description'=>$data['meta_description'],
            'parent_id'=>$data['parent_id'],
            'image'=>$data['image'],
            'status'=>$data['status'],
            'top'=>$data['top'],
        ];

        $this->update($id,$category);
        if($data['store_id'])
        {
            $category = $this->getById($id);
            $this->repo->storeCategoryInStores($data['store_id'],$category);
        }

    }

    /**
     * Get Products By Category Id
     * 
     * @param $categoryId
     * @return mixed
     */
    public function getProductByCategoryId($categoryId)
    {
        return $this->repo->getProductsByCategoryId($categoryId);
    }

    /**
     * Get Parent Category
     *
     * 
     * @return mixed
     */

    public function parentCategory()
    {
        return $this->repo->parentCategory();
    }
    

    /**
     * Get Top Category
     *
     * 
     * @return mixed
     */
    public function topCategory()
    {
        return $this->repo->topCategory();
    }

     /**
     * Get Products by Category Slug
     *
     * @param $slug
     * @return mixed
     */
    public function getCategoryByCategorySlug($slug)
    {
        return $this->repo->getCategoryByCategorySlug($slug);
    }

    /**
     * Get Product With Category
     * @param storeId
     * @param products
     * @return mixed
     */
    
    public function getCategorylist($storeId)
    {
        return $this->repo->getCategorylist($storeId);
    }


    
}