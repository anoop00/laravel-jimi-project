<?php
/**
 * Created by PhpStorm.
 * User: Khem
 */

namespace AppRepo\Providers;

use Illuminate\Support\ServiceProvider;

/**
 * Class RepositoryServiceProvider
 * @package AppRepo\Providers
 * @author Khem Raj Regmi <khemrr067@gmail.com>
 */
class RepositoryServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(
            'AppRepo\Repository\CategoryRepositoryInterface',
            'AppRepo\Repository\Eloquent\CategoryEloquentRepository'
        );
        $this->app->bind(
            'AppRepo\Repository\ProductRepositoryInterface',
            'AppRepo\Repository\Eloquent\ProductEloquentRepository'
        );
        $this->app->bind(
            'AppRepo\Repository\ManufacturerRepositoryInterface',
            'AppRepo\Repository\Eloquent\ManufacturerEloquentRepository'
        );
        $this->app->bind(
            'AppRepo\Repository\BannerRepositoryInterface',
            'AppRepo\Repository\Eloquent\BannerEloquentRepository'
        );
        $this->app->bind(
            'AppRepo\Repository\BannerImageRepositoryInterface',
            'AppRepo\Repository\Eloquent\BannerImageEloquentRepository'
        );
        $this->app->bind(
            'AppRepo\Repository\SettingRepositoryInterface',
            'AppRepo\Repository\Eloquent\SettingEloquentRepository'
        );
    }
}