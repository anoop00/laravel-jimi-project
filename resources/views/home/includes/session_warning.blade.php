<script type="text/javascript">
    iziToast.warning({
        title: 'Caution',
        message: '{{ Session::get('warning') }}',
        position: 'topRight'
    });
</script>