<div class="side-menu animate-dropdown outer-bottom-xs">
    <div class="head"><i class="icon fa fa-align-justify fa-fw"></i> Categories</div>        
    <nav class="yamm megamenu-horizontal" role="navigation">
    	<ul class="nav">
        @if(!empty($parent_categories))
        {{-- {{dd($parent_categories)}} --}}
            @foreach($parent_categories as $category)
            @if(!($category->children->isEmpty()))
    		<li class="dropdown menu-item">
    			<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon fa fa-shopping-bag" aria-hidden="true"></i>{{$category->name}}</a>
    			<ul class="dropdown-menu mega-menu">
    				<li class="yamm-content">
    					<div class="row">
    						<div class="col-sm-12 col-md-3">
    							<ul class="links list-unstyled">

                                
                                @foreach ($category->children as $children)
    								<li><a href="{{route('category.product',['category'=>$category->slug,'product'=>$children->slug])}}">{{$children->name}}</a></li>
    							@endforeach	
    							</ul>
    						</div><!-- /.col -->
    						
    					</div><!-- /.row -->
    				</li><!-- /.yamm-content -->                    
    			</ul><!-- /.dropdown-menu -->            
    		</li><!-- /.menu-item -->

            @endif
            @endforeach
        @endif
		</ul><!-- /.nav -->
	</nav><!-- /.megamenu-horizontal -->
</div><!-- /.side-menu -->