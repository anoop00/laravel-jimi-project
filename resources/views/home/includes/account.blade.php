@extends('home.layouts.master_layout')

    @section('pageTitle')
        <span>My Account</span>
    @endsection
    
    @section('breadcrumbs')
          <li><a href="#">Account</a></li>
          <li><a href="#">{{ Auth::user()->firstname }} {{ Auth::user()->lastname }}</a></li>
    @endsection

    

    @section('sidebar')
        @include('home.customer.partials.sidebar')
    @endsection

    @section('content')   
        @if(Session::has('error'))
            @include('home.customer.partials.session_error')
        @elseif(Session::has('success'))
            @include('home.customer.partials.session_success')
        @elseif(Session::has('warning'))
            @include('home.customer.partials.session_warning')
        @endif 
        {{-- <h2 class="product-heading padding-bottom-10">My Account --}}
        

        <!--Account Start Here-->
            @include('home.customer.partials.account_info')
        <!-- Account End Here -->

        <!--Address Modal -->
            @include('home.customer.partials.account_address_modal')

        <!--Address Edit Modal -->
            @include('home.customer.partials.account_edit_address_modal')

        <!--Edit Information Modal -->
            @include('home.customer.partials.account_edit_modal')
    @endsection


    @section('scripts')
        <!-- PNotify -->
        <script src="{{asset('assets/admin/vendors/pnotify/dist/pnotify.js')}}"></script>
        <script src="{{asset('assets/admin/vendors/pnotify/dist/pnotify.buttons.js')}}"></script>
        <script src="{{asset('assets/admin/vendors/pnotify/dist/pnotify.nonblock.js')}}"></script>
        @include('laravelPnotify::notify')
        

        {{-- <script type="text/javascript" src="{{asset('assets/home/js/common_function.js')}}"></script> --}}
        <script type="text/javascript">

        // $(document).ready(function(){

            // $("#myModal").modal('show');
        // });
            CountryStateCityChainMethod();  

           function CountryStateCityChainMethod (){
            $('.input-country_id').change(function()
                {
                    $.get('{{ url('admin/information') }}/' + this.value + '/states', function(cities)
                    {
                        var $state = $('.input-state_id');

                        $state.find('option').remove().end();

                        $.each(cities, function(index, city) {
                            console.log(city);
                            $state.append('<option value="' + city.state_id + '">' + city.name + '</option>');
                        });
                    });
                });

                $('.input-state_id').change(function()
                {
                    $.get('{{ url('admin/information') }}/' + this.value + '/cities', function(cities)
                    {
                        var $city = $('.input-city_id');

                        $city.find('option').remove().end();

                        $.each(cities, function(index, city) {
                            $city.append('<option value="' + city.city_id + '">' + city.name + '</option>');
                        });
                    });
                });

                $('.input-city_id').change(function()
                {
                    $.get('{{ url('admin/information') }}/' + this.value + '/areas', function(areas)
                    {
                        var $area = $('.input-area_id');

                        $area.find('option').remove().end();

                        $.each(areas, function(index, area) {
                            $area.append('<option value="' + area.area_id + '">' + area.name + '</option>');
                        });
                    });
                });
            }      
        </script>

        <script type="text/javascript">
            // $(".editAddress").on("click", function(res){
            //         res.preventDefault();
            //         var addressID = $(this).data('addressid');
            //         // console.log('hello');
                    
            //     });
            $('#addressedit_response').hide();
            $(".editAddress").on("click",function(res){
                res.preventDefault();
                var firstname = $(this).data('firstname');
                var lastname=$(this).data('lastname');
                var company = $(this).data('company');
                var address_1=$(this).data('address_1');
                var telephone=$(this).data('telephone');
                var address_2=$(this).data('address_2');
                var country_id =$(this).data('countryid');
                var state_id=$(this).data('stateid');
                var city_id=$(this).data('cityid');
                var area_id = $(this).data('areaid');
                var country_name =$(this).data('countryname');
                var state_name=$(this).data('statename');
                var city_name=$(this).data('cityname');
                var area_name = $(this).data('areaname');
                var addressid = $(this).data('addressid');

                // console.log(firstname);
                // console.log(lastname);
                // console.log(company);
                // console.log(telephone);
                // console.log(address_1);
                // console.log(address_2);
                // console.log(country_id);
                // console.log(state_id);
                // console.log(city_id);
                // console.log(area_id);
                // console.log(state_name);
                // console.log(city_name);
                // console.log(area_name);
                // console.log(addressid);
                // return;

                
                $("#editAddressModel").modal('show');
                $("#input_edit_address-firstname").val(firstname);
                $("#input_edit_address-lastname").val(lastname);
                $("#input_edit_address-company").val(company);
                $("#input_edit_address-telephone").val(telephone);
                $("#input_edit_address-address_1").val(address_1);
                $("#input_edit_address-address_2").val(address_2);
                $('#input_edit_address-country_id option').prop('selected', false);

                $('#input_edit_address-country_id option').filter(function() { 
                    return ($(this).text() == country_name); 
                }).prop('selected', true);

                $('#input_edit_address-state_id').html($('<option>', {
                    value: state_id,
                    text:  state_name
                }));
                $('#input_edit_address-city_id').html($('<option>', {
                    value: city_id,
                    text:  city_name
                }));
                $('#input_edit_address-area_id').html($('<option>', {
                    value: area_id,
                    text:  area_name
                }));

                $("#UpdateAddress").on("click", function (res) {
                    res.preventDefault();
                    // for (i in data.error){
                    //     var element = $('#input_edit_address-' + i);
                    //     $(element).parent().addClass('has-error');
                    //     $(element).hide();
                    // }
                    var firstname = $('#input_edit_address-firstname').val();
                    var lastname = $('#input_edit_address-lastname').val();
                    var company = $('#input_edit_address-company').val();
                    var telephone =  $('#input_edit_address-telephone').val();
                    var address_1 = $('#input_edit_address-address_1').val();
                    var address_2 = $('#input_edit_address-address_2').val();
                    var country_id =  $('#input_edit_address-country_id option:selected').val();
                    var state_id =  $('#input_edit_address-state_id option:selected').val();
                    var city_id = $('#input_edit_address-city_id option:selected').val();
                    var area_id = $('#input_edit_address-area_id option:selected').val();
                    var Url = "{{route('customer.update_address')}}";
                    var token ="{{csrf_token()}}";

                    // console.log(firstname);
                    // console.log(lastname);
                    // console.log(company);
                    // console.log(telephone);
                    // console.log(address_1);
                    // console.log(address_2);
                    // console.log(country_id);
                    // console.log(state_id);
                    // console.log(city_id);
                    // console.log(area_id);

                     // console.log(addressid);
                     // return;

                    /**Get Data ***/
                            $.ajax({
                                type: "POST",
                                url: Url,
                                data:{
                                    "address_id":addressid,
                                    "_token": token,
                                    "firstname":firstname,
                                    "lastname":lastname,
                                    "company":company,
                                    "telephone":telephone,
                                    "address_1":address_1,
                                    "address_2":address_2,
                                    "country_id":country_id,
                                    "state_id":state_id,
                                    "city_id":city_id,
                                    "state_id":state_id,
                                    "area_id":area_id,
                                },
                                beforeSend: function() { $('#addressedit_response').show(); },
                                success: function(data){
                                    console.log(data.message);
                                   if(data.message=='success') {
                                    $('#addressedit_response').hide();
                                    iziToast.success({
                                        title: 'OK',
                                        message: 'Your Address Updated Sucessfully ...',
                                        position: 'topRight',
                                    });
                                    setTimeout(function(){// wait for 5 secs(2)
                                        location.reload(); // then reload the page.(3)
                                    }, 2000);
                                   }
                                   if(data.error)
                                   {
                                        for (i in data.error){
                                            $('.text-danger').hide();
                                        }
                                    $('#addressedit_response').hide();
                                    console.log(data);
                                    for (i in data.error){
                                        var element = $('#input_edit_address-' + i);
                                        $(element).parent().addClass('has-error');
                                        $(element).after('<div class="text-danger">' + data.error[i] + '</div>');
                                    }
                                   }
                                    // $(".wishlist").html(wishlist); 
                                }
                            });

                        /** End Data **/
                });
                
            });


            $(".deleteAddress").on("click", function(res){
                    res.preventDefault();
                    var addressID = $(this).data('addressid');
                    var token = $(this).data('token');
                    var Url = "{{route('customer.delete-address')}}";

                    iziToast.show({
                    color: 'dark',
                    icon: 'fa fa-user',
                    title: 'Hey',
                    message: 'Are you sure that you want to delete this Address!',
                    position: 'center', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter
                    progressBarColor: 'rgb(0, 255, 184)',
                    buttons: [
                      [
                        '<button>Ok</button>',
                        function (instance, toast) {
                        
                         /** Delete data ***/
                            $.ajax({
                                type: "GET",
                                url: Url,
                                data:{
                                    "_token":token,
                                    "addressID":addressID,
                                },
                                success: function(msg){
                                  iziToast.success({
                                        title: 'OK',
                                        message: 'Your Address successfully Removed',
                                        position: 'topRight',
                                    });  
                                }
                                    
                            });
                            instance.hide({
                                transitionOut: 'fadeOutUp'
                              }, toast);

                            setTimeout(function(){// wait for 5 secs(2)
                                location.reload(); // then reload the page.(3)
                            }, 2000);
                        }
                      ],
                      [
                        '<button>Close</button>',
                        function (instance, toast) {
                          instance.hide({
                            transitionOut: 'fadeOutUp'
                          }, toast);
                        }
                      ]
                    ]
                  });
                });
        </script>

        <script>
                function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#blah')
                            .attr('src', e.target.result);
                    };

                    reader.readAsDataURL(input.files[0]);
                }
            }

            $(document).on('click', '#close-preview', function(){ 
            $('.image-preview').popover('hide');
            // Hover befor close the preview    
                });

            $(function() {
                // Create the close button
                var closebtn = $('<button/>', {
                    type:"button",
                    text: 'x',
                    id: 'close-preview',
                    style: 'font-size: initial;',
                });
                closebtn.attr("class","close pull-right");

                // Clear event
                $('.image-preview-clear').click(function(){
                    $('.image-preview').attr("data-content","").popover('hide');
                    $('.image-preview-filename').val("");
                    $('.image-preview-clear').hide();
                    $('.image-preview-input input:file').val("");
                    $(".image-preview-input-title").text("Browse"); 
                     $('#blah').attr('src', '{{asset('')}}uploads/customer_image/<?php echo Auth::user()->image ; ?>');
                }); 
                // Create the preview image
                $(".image-preview-input input:file").change(function (){     
                    var img = $('<img/>', {
                        id: 'dynamic',
                        width:250,
                        height:200
                    });      
                    var file = this.files[0];
                    var reader = new FileReader();
                    // Set preview image into the popover data-content
                    reader.onload = function (e) {
                        $(".image-preview-input-title").text("Change");
                        $(".image-preview-clear").show();
                        $(".image-preview-filename").val(file.name);
                    }        
                    reader.readAsDataURL(file);
                });  
            });
        </script>

        <script type="text/javascript">

            $(".updateInfo").on("click", function (res) {
                res.preventDefault();
                var firstname = $('#input_info-firstname').val();
                var lastname = $('#input_info-lastname').val();
                var email = $('#input_info-email').val();
                var telephone =  $('#input_info-telephone').val();
                var Url = "{{route('customer.update')}}";
                var token ="{{csrf_token()}}";
                $('#response').hide();

                // console.log(firstname);
                // console.log(lastname);
                // console.log(email);
                // console.log(telephone);

                /**Get Data ***/
                        $.ajax({
                            type: "POST",
                            url: Url,
                            data:{
                                "_token": token,
                                "firstname":firstname,
                                "lastname":lastname,
                                "email":email,
                                "telephone":telephone,
                            },
                            beforeSend: function() { $('#response').show(); },
                            success: function(data){
                               // console.log(data.message);
                               if(data.message=='success') {
                                iziToast.success({
                                    title: 'OK',
                                    message: 'Your Information Updated Sucessfully ...',
                                    position: 'topRight',
                                });
                                $('#response').hide();
                                setTimeout(function(){// wait for 5 secs(2)
                                    location.reload(); // then reload the page.(3)
                                }, 2000);
                               }
                               if(data.error)
                               {
                                for (i in data.error){
                                            $('.text-danger').hide();
                                        }
                                $('#response').hide();
                                // console.log(data);
                                for (i in data.error){
                                    var element = $('#input_info-' + i);
                                    $(element).parent().addClass('has-error');
                                    $(element).after('<div class="text-danger">' + data.error[i] + '</div>');
                                }
                               }
                                
                                
                            }
                           
                        });

                        /** End Data **/
            });

            $('#address_response').hide();
            $("#addAddress").on("click", function (res) {
                res.preventDefault();
                
                var firstname = $('#input_add_address-firstname').val();
                var lastname = $('#input_add_address-lastname').val();
                var company = $('#input_add_address-company').val();
                var telephone =  $('#input_add_address-telephone').val();
                var address_1 = $('#input_add_address-address_1').val();
                var address_2 = $('#input_add_address-address_2').val();
                var country_id =  $('#input_add_address-country_id option:selected').val();
                var state_id =  $('#input_add_address-state_id option:selected').val();
                var city_id = $('#input_add_address-city_id option:selected').val();
                var area_id = $('#input_add_address-area_id option:selected').val();
                var Url = "{{route('customer.add_address')}}";
                var token ="{{csrf_token()}}";

                // console.log(firstname);
                // console.log(lastname);
                // console.log(company);
                // console.log(telephone);
                // console.log(address_1);
                // console.log(address_2);
                // console.log(country_id);
                // console.log(state_id);
                // console.log(city_id);
                // console.log(area_id);
                /**Get Data ***/
                        $.ajax({
                            type: "POST",
                            url: Url,
                            data:{
                                "_token": token,
                                "firstname":firstname,
                                "lastname":lastname,
                                "company":company,
                                "telephone":telephone,
                                "address_1":address_1,
                                "address_2":address_2,
                                "country_id":country_id,
                                "state_id":state_id,
                                "city_id":city_id,
                                "state_id":state_id,
                                "area_id":area_id,
                            },
                            beforeSend: function() { $('#address_response').show(); },
                            success: function(data){
                                console.log(data.message);
                               if(data.message=='success') {
                                $('#address_response').hide();
                                iziToast.success({
                                    title: 'OK',
                                    message: 'Your Address Added Sucessfully ...',
                                    position: 'topRight',
                                });
                                setTimeout(function(){// wait for 5 secs(2)
                                    location.reload(); // then reload the page.(3)
                                }, 2000);
                               }
                               if(data.error)
                               {
                                for (i in data.error){
                                            $('.text-danger').hide();
                                        }
                                $('#address_response').hide();
                                console.log(data);
                                for (i in data.error){
                                    var element = $('#input_add_address-' + i);
                                    
                                    $(element).parent().addClass('has-error');
                                    $(element).after('<div class="text-danger">' + data.error[i] + '</div>');
                                }
                               }
                                
                                // $(".wishlist").html(wishlist); 
                              
                            }
                        });

                        /** End Data **/
            });


        </script>

    @endsection