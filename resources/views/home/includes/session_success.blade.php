<script type="text/javascript">
    iziToast.success({
        title: 'Success',
        message: '{{ Session::get('success') }}',
        position: 'topRight'
    });
</script>