@extends('home.layouts.master')

    @section('content')
<section id="faq" class="portfolio">
    <div class="container-fluid">
       <!-- section title -->
<div class="row-fluid">
    <div class="span12">
        <div class="section-title">
            <h2 style="text-align: center;">
                <span>Frequently Asked Questions</span>
            </h2>
        </div>
    </div>
</div>
<!-- section title end --> 
      <div class="row-fluid">
            <div align="center" class="span12">
                <div class="section-subtitle">
                    <h2 style="color: #176CB0;">Answers of your Possible Questions? </h2>
                </div>
            </div>
        </div>                        

  <div class="container">
            <div class="row">                                                                  
                <div class="span6">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" href="#collapse1" aria-expanded="true" aria-controls="collapseOne" class="trigger collapsed">
                                    1. Why should we opt for Organic Fertilizer which is expensive compared to Chemical Fertilizer?
                            </a>
                          </h4>
                        </div>
                        <div id="collapse1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <p><p>Chemical fertilizer have high acid content which has the ability to burn skin and change the status of the fertility of the soil. However, organic fertilizer adds natural nutrients to soil, increases soil organic matter, improves soil structure, improves water holding capacity, reduces soil crusting problems and reduces erosion from wind and water.</p></p>
                          </div>
                        </div>
                      </div>
                </div>
                                                                  
                <div class="span6">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" href="#collapse2" aria-expanded="true" aria-controls="collapseOne" class="trigger collapsed">
                                  2. What makes OBiFert better than other fertilizers in the market?
                            </a>
                          </h4>
                        </div>
                        <div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <p><p>OBiFert fertilizer is produced using hot composting method which kill weed seeds and prevents diseases like pathogens, nematode etc. Additionally, the product goes through quality check tests to ensure no inorganic materials get mix-up during the process of composting and packaging. Likewise, the demo tests in different crops and places have proved, OBiFert to be a highly effective plant food.</p></p>
                          </div>
                        </div>
                      </div>
                </div>
                                                                  
                <div class="span6">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" href="#collapse3" aria-expanded="true" aria-controls="collapseOne" class="trigger collapsed">
                                    3. Where can OBiFert fertilizer be purchased from?
                            </a>
                          </h4>
                        </div>
                        <div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <p><p>OBiFert fertilizer can be purchased from Vets, retail shops, OBiFert appointed dealers or nurseries. Meanwhile, for the dealership/stockiest, please contact OBiFert Sales department.</p></p>
                          </div>
                        </div>
                      </div>
                </div>
                                                                  
                <div class="span6">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" href="#collapse4" aria-expanded="true" aria-controls="collapseOne" class="trigger collapsed">
                                    4. Is OBiFert fertilizer good for vegetables?
                            </a>
                          </h4>
                        </div>
                        <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <p><p>Yes, OBiFert fertilizer is proved to be effective for vegetables. Rather, it has been proved to be highly effective for other crops and flowers as well.</p></p>
                          </div>
                        </div>
                      </div>
                </div>
                                                                  
                <div class="span6">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" href="#collapse5" aria-expanded="true" aria-controls="collapseOne" class="trigger collapsed">
                                    5. Is OBiFert fertilizer fully organic?
                            </a>
                          </h4>
                        </div>
                        <div id="collapse5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <p><p>Yes, OBiFert fertilizer is fully organic as the raw materials used to produce it do not content any inorganic matters. Animal manures used for the purpose of composting are lab tested..</p></p>
                          </div>
                        </div>
                      </div>
                </div>
                                                                  
                <div class="span6">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" href="#collapse6" aria-expanded="true" aria-controls="collapseOne" class="trigger collapsed">
                                    6. Is it necessary to test soil before using the fertilizer?
                            </a>
                          </h4>
                        </div>
                        <div id="collapse6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <p><p>Testing of soil before the use of fertilizer will certainly help to know about the fertility and status of the soil. However, the testing of soil is not compulsory before the use of fertilizer.</p></p>
                          </div>
                        </div>
                      </div>
                </div>
                                                                  
                <div class="span6">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" href="#collapse7" aria-expanded="true" aria-controls="collapseOne" class="trigger collapsed">
                                    7. Does the use of animal manures to make OBiFert product result in negative impact on the soil/ plant?
                            </a>
                          </h4>
                        </div>
                        <div id="collapse7" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <p><p>Researches have proved that use of animal manures to make organic fertilizers are far better than the household wastes and other resources as it contains required level of nutrient values. Additionally, animal manures are already widely used in the country for the purpose of farming. In this case, the use of animal manures to make the fertilizer further enhances its strength.</p></p>
                          </div>
                        </div>
                      </div>
                </div>
                                                                  
                <div class="span6">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" href="#collapse8" aria-expanded="true" aria-controls="collapseOne" class="trigger collapsed">
                                    8. Is OBiFert certified for Organic production?
                            </a>
                          </h4>
                        </div>
                        <div id="collapse8" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <p><p>Yes, OBiFert products are NOP (USA), NPOP (Europe) and COS (Canada) certified.</p></p>
                          </div>
                        </div>
                      </div>
                </div>
                                                                  
                <div class="span6">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" href="#collapse9" aria-expanded="true" aria-controls="collapseOne" class="trigger collapsed">
                                    9. Can OBiFert fertilizer be mixed with other fertilizers?
                            </a>
                          </h4>
                        </div>
                        <div id="collapse9" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <p><p>It is recommended to not use the mixture of fertilizers. Rather, it is suggested to first check the quality of each fertilizer and then use the one with best results.</p></p>
                          </div>
                        </div>
                      </div>
                </div>
                                                                  
                <div class="span6">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" href="#collapse10" aria-expanded="true" aria-controls="collapseOne" class="trigger collapsed">
                                   10. Will the fertilizer effect the plant if used in excessive quantity?
                            </a>
                          </h4>
                        </div>
                        <div id="collapse10" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <p><p>No, the excess use of fertilizer will not affect the plant even if used in excess quantity.</p></p>
                          </div>
                        </div>
                      </div>
                </div>
            </div>
        </div>
            
    </div><!-- end of container --> 
</section><!-- end of sections -->
@endsection