    <div class="main-header">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-3 logo-holder">
                    <!-- ============================================================= LOGO ============================================================= -->
                    <div class="logo">
                        <a href="home.html">
                            <img src="{{asset('logo/jimigrouplogo.png')}}" alt="">
                        </a>
                    </div><!-- /.logo -->
                    <!-- ============================================================= LOGO : END ============================================================= -->             
                </div><!-- /.logo-holder -->
                <div class="col-xs-12 col-sm-12 col-md-3"></div>
                <div style="padding-top: 5%" class="col-xs-12 col-sm-12 col-md-6 top-search-holder">
                    <!-- /.contact-row -->
                    <!-- ============================================================= SEARCH AREA ============================================================= -->
                    <div class="search-area">
                        <form>
                            <div class="control-group">

                                <input class="search-field" placeholder="Search here..." />

                                <a class="search-button" href="#" ></a>    

                            </div>
                        </form>
                    </div><!-- /.search-area -->
                    <!-- ============================================================= SEARCH AREA : END ============================================================= -->
                </div><!-- /.top-search-holder -->
            </div><!-- /.row -->

        </div><!-- /.container -->

    </div><!-- /.main-header -->

    <!-- ============================================== NAVBAR ============================================== -->
    <div class="header-nav animate-dropdown">
        <div class="container">
            <div class="yamm navbar navbar-default" role="navigation">
                <div class="navbar-header">
                    <button data-target="#mc-horizontal-menu-collapse" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="nav-bg-class">
                    <div class="navbar-collapse collapse" id="mc-horizontal-menu-collapse">
                    <div class="nav-outer">
                        <ul class="nav navbar-nav">                          
                                <li>
                                    <a href="category.html" tabindex="-1" data-hover="dropdown" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon fa fa-align-justify fa-fw"></i> Category</a>
                                    <ul class="dropdown-menu">
                                            @if(!empty($parent_categories))
                                        {{-- {{dd($parent_categories)}} --}}
                                            @foreach($parent_categories as $category)
                                            @if(!($category->children->isEmpty()))
                                            <li class="dropdown-submenu menu-item">
                                                <a href="#" class="test" tabindex="-1" data-toggle="dropdown"><i class="icon fa fa-shopping-bag" aria-hidden="true"></i> {{$category->name}}</a>
                                                 <ul class="dropdown-menu mega-menu">
                                                    <li class="yamm-content">
                                                        <div class="row">
                                                            <div class="col-sm-12 col-md-12">
                                                                <ul class="links list-unstyled">
                                                                    @foreach ($category->children as $children)  
                                                                        <li class="child_list"><a href="{{route('category.product',['category'=>$category->slug,'product'=>$children->slug])}}">{{$children->name}}</a></li>
                                                                    @endforeach 
                                                                </ul>
                                                            </div>
                                                        </div><!-- /.row -->
                                                    </li><!-- /.yamm-content -->                    
                                                </ul><!-- /.dropdown-menu -->            
                                            </li><!-- /.menu-item -->
                                            @endif
                                            @endforeach
                                            @endif                                           
                                    </ul>
                                </li>

                                <li class="">
                                    <a href="{{url('/aboutus')}}" >About Us</a>
                                </li>
                                <li class="">
                                    <a href="{{url('/faqs')}}" >FAQs</a>
                                </li>
                                <li class="">
                                    <a href="{{url('/contactus')}}">Contact Us</a>
                                </li>                                               
                        </ul><!-- /.navbar-nav -->
                        <div class="clearfix"></div>                
                    </div><!-- /.nav-outer -->
            </div><!-- /.navbar-collapse -->


            </div><!-- /.nav-bg-class -->
        </div><!-- /.navbar-default -->
    </div><!-- /.container-class -->

</div>