<div class="side-menu animate-dropdown outer-bottom-xs">
    <div class="head"><i class="icon fa fa-align-justify fa-fw"></i> My Account</div>        
    <nav class="yamm megamenu-horizontal" role="navigation">
    	<ul class="nav">
    		<li><a href="{{route('customer.dashboard')}}"><i class="fa fa-fw fa-user"></i><span>User Information</span></a></li>
            <li><a href="{{route('customer.order',Auth::user()->customer_id)}}"><i class="fa fa-fw fa-history"></i><span>Order History</span></a></li>
            <li><a href="{{route('customer.address')}}"><i class="fa fa-fw fa-map-marker"></i><span>Address</span></a></li>
            <li><a href="{{route('customer.wishlist',Auth::user()->customer_id)}}"><i class="fa fa-fw fa-th-list"></i><span>Wishlist</span></a></li>
		</ul><!-- /.nav -->
	</nav><!-- /.megamenu-horizontal -->
</div><!-- /.side-menu -->