@extends('home.layouts.master')
@section('content')
    @if(Session::has('flash_message'))
        <script>
        iziToast.info({
            title: '',
            message: '{!! session('flash_message') !!}',
            position: 'topRight'
        });
    </script>
       {{--  <div class="alert alert-success"><span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em></div> --}}
    @endif
    

    <div class="body-content outer-top-xs" id="top-banner-and-menu">
        <div class="container">
        <div class="row">
        <!-- ============================================== SIDEBAR ============================================== -->  
            <div class="col-xs-12 col-sm-12 col-md-3 sidebar"> 
                @include('home.includes.sidemenu')
    <!-- ================================== TOP NAVIGATION : END ================================== -->

                @include('home.includes.sidebanner') 
            </div><!-- /.sidemenu-holder -->
            <!-- ============================================== SIDEBAR : END ============================================== -->

            <!-- ============================================== CONTENT ============================================== -->
            
            <div class="col-xs-12 col-sm-12 col-md-9 homebanner-holder">
                
                <div id="product-tabs-slider" class="scroll-tabs outer-top-vs wow fadeInUp">
                {{-- {{ dd($categoryWiseProduct) }} --}}
                
                        {{-- {{ dd($category->products) }} --}}
                      
                        <div class="more-info-tab clearfix ">
                           <h3 class="new-product-title pull-left">Search Result</h3>
                        </div>
                        <div class="tab-content outer-top-xs">
                            <div class="tab-pane in active" id="all">           
                                <div class="product-slider">
                                    <div class="owl-carousel home-owl-carousel custom-carousel owl-theme" data-item="4">
                                    @if(!empty($products))
                                        @foreach($products as $product)      
                                        <div class="item item-carousel">
                                            <div class="products">
                                            {{-- {{ dd($category->products) }} --}}  
                                            <div class="product">       
                                                <div class="product-image">
                                                    <div class="image">
                                                        <a href="{{route('single.product',$product->slug)}}"
                                                             data-id="{{$product->product_id}}"><img  src="{{asset($product->image)}}" alt="{{$product->image}}"></a>
                                                    </div><!-- /.image -->          

                                                    <div class="tag new"><span>new</span></div>                                
                                                </div><!-- /.product-image -->
                                                    
                                                
                                                <div class="product-info text-left">
                                                    <h3 class="name"><a href="detail.html">{{$product->name}}</a></h3>
                                                    <div class="rating rateit-small"></div>
                                                    <div class="description"></div>

                                                    <div class="product-price"> 
                                                        <span class="price">Rs {{round($product->price, 2)}} </span>
                                                        {{-- <span class="price-before-discount">$ 800</span> --}}
                                                                            
                                                    </div><!-- /.product-price -->
                                                    
                                                </div><!-- /.product-info -->
                                                <div class="cart clearfix animate-effect">
                                                    <div class="action">
                                                        <ul class="list-unstyled">
                                                            <li class="add-cart-button btn-group "  data-productid="{{$product->product_id}}" data-token="{{csrf_token()}}"type="button">
                                                                <button data-toggle="tooltip" class="btn btn-primary icon" type="button" title="Add Cart">
                                                                    <i class="fa fa-shopping-cart"></i>                                                 
                                                                </button>
                                                                <button class="btn btn-primary cart-btn" 
                                                               >Add to cart</button>
                                                                                        
                                                            </li>
                                                            @if(Auth::check())
                                                            <li class="lnk wishlist wish"  data-productid="{{$product->product_id}}" data-customerid="{{Auth::user()->customer_id}}"
                                                                data-token="{{csrf_token()}}">
                                                                <a data-toggle="tooltip" class="add-to-cart" href="detail.html" title="Wishlist">
                                                                     <i class="icon fa fa-heart"></i>
                                                                </a>
                                                            </li>
                                                            @else
                                                            <li class="lnk wishlist wish"
                                                                data-productid="{{$product->product_id}}" data-customerid="login"
                                                                data-token="{{csrf_token()}}" >
                                                                <a data-toggle="tooltip" class="add-to-cart" href="detail.html" title="Wishlist">
                                                                     <i class="icon fa fa-heart"></i>
                                                                </a>
                                                            </li>
                                                            @endif

                                                            <li class="lnk compare"
                                                                data-productid="{{$product->product_id}}"
                                                                data-token="{{csrf_token()}}">
                                                                <a data-toggle="tooltip" class="add-to-cart" href="detail.html" title="Compare">
                                                                    <i class="fa fa-signal" aria-hidden="true"></i>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div><!-- /.action -->
                                                </div><!-- /.cart -->
                                            </div><!-- /.product -->
                                      
                                            </div><!-- /.products -->
                                        </div><!-- /.item -->
                                        @endforeach
                                    @else
                                        <div class="item item-carousel">
                                            <div class="products">
                                            Sorry !! your search product could'nt be found
                                            </div><!-- /.products -->
                                        </div><!-- /.item -->
                                    @endif  
                                    </div><!-- /.home-owl-carousel -->
                                </div><!-- /.product-slider -->
                            </div><!-- /.tab-pane -->

                        </div><!-- /.tab-content -->

                </div><!-- /.scroll-tabs -->
            </div><!-- /.homebanner-holder -->
 
            <!-- ================================ CONTENT : END ======================== -->
        </div><!-- /.row -->
            <!-- ====================================== BRANDS CAROUSEL ========================== -->
                    {{-- @include('home.includes.brandslider') --}}
        <!-- ===================================== BRANDS CAROUSEL : END ========================== -->
        </div><!-- /.container -->
    </div><!-- /#top-banner-and-menu -->    
@stop
@section('pageScript')

<!-- Datatables -->
<script type="text/javascript" src="{{asset('packages/barryvdh/elfinder/js/standalonepopup.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets/home/js/cart.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/home/js/wishlist.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/home/js/compare.js') }}"></script>
<script type="text/javascript">
    $(".compare").on("click", function(res){
        res.preventDefault();
        var productid = $(this).attr('data-productid');
        var Url = "{{route('addCompare')}}";
        var token = $(this).data('token');
        Compare(Url, token, productid);
    })
</script>
<script type="text/javascript">
   
    $('.add-cart-button').each(function (i, el) {
        $(el).click(function (e) {
            var productid = $(el).data('productid');
            var token = $(el).data('token');
            var Url = "{{route('addcart')}}";
            var cartUrl= "{{route('cart')}}";
            var checkoutUrl="{{route('checkout')}}";
            var order_CancelUrl="{{route(' ')}}";
            var imgUrl = "{{ asset('') }}";
            CartClick(productid, token, Url, cartUrl, checkoutUrl,order_CancelUrl,imgUrl);
            e.preventDefault();
            e.stopPropagation();
        });
    });
</script>
{{-- <script type="text/javascript">
    $(".product-permalink").on("click", function (res) {
        var id = $(this).data('id');
        onProductClick(id);
    });
    function onProductClick(id) {
        console.log("PRoduct Click " + id);
        analytic.trackProductClick(productsJson, id);
    }
</script> --}}
<script type="text/javascript">
    $(".wish").on("click", function (res) {
        res.preventDefault();
        var productid = $(this).attr('data-productid');
        var customerId = $(this).attr('data-customerid');
        if(customerId=='login')
        {
            iziToast.error({
                title: 'Error',
                message: 'please login to add the product in wishlist....',
            });
            // alert('please login to add the product in wishlist.....');
            return;
        }
        var Url = "{{route('customer.addwishlist')}}";
        var token = $(this).data('token');
        WishListClick(Url, token, productid, customerId);
    });
</script>
@endsection



