@extends('home.layouts.master')

@section('content')
	<div class="body-content outer-top-xs" id="top-banner-and-menu">
        <div class="container">
        <div class="row">
        <!-- ============================================== SIDEBAR ============================================== -->  
            <div class="col-xs-12 col-sm-12 col-md-3 sidebar"> 
            <!-- ================================== TOP NAVIGATION ================================== -->
				{{-- @include('home.includes.sidemenu') --}}
			<!-- ================================== TOP NAVIGATION : END ================================== -->

				{{-- @include('home.includes.sidebanner') --}}
            </div><!-- /.sidemenu-holder -->
            <div class="col-xs-12 col-sm-12 col-md-12 homebanner-holder">
            	@include('home.includes.banner')
	            
		
				<!-- ========= SECTION – HERO : END ======== -->	

				<!-- =========== INFO BOXES ========= -->
					{{-- @include('home.includes.infoboxes') --}}
				<!-- ============ INFO BOXES : END ================ -->
				<!-- ============= SCROLL TABS =============== -->
					{{-- @include('home.includes.productlist') --}}
					<div class="col-xs-12 col-sm-12 col-md-12 homebanner-holder">
		                @if(!empty($categoryWiseProduct))
		                @foreach($categoryWiseProduct as $category)   
		                <div id="product-tabs-slider" class="scroll-tabs outer-top-vs wow fadeInUp">
		                {{-- {{ dd($categoryWiseProduct) }} --}}
		                
		                        {{-- {{ dd($category->products) }} --}}
		                    @if(!empty($category->products))    
		                        <div class="more-info-tab clearfix ">
		                           <h3 class="new-product-title pull-left">{{ $category->name }}</h3>
		                        </div>
		                        <div class="tab-content outer-top-xs">
		                            <div class="tab-pane in active" id="all">           
		                                <div class="product-slider">
		                                    <div class="owl-carousel home-owl-carousel custom-carousel owl-theme" data-item="4">
		                                        @foreach($category->products as $product)      
		                                        <div class="item item-carousel">
		                                            <div class="products">
		                                            {{-- {{ dd($category->products) }} --}}  
		                                            <div class="product">       
		                                                <div class="product-image">
		                                                    <div class="image">
		                                                        <a href="{{route('single.product',$product->slug)}}"
		                                                             data-id="{{$product->id}}"><img  src="{{asset($product->image)}}" alt="{{$product->image}}"></a>
		                                                    </div><!-- /.image -->          

		                                                    {{-- <div class="tag new"><span>new</span></div>   --}}                              
		                                                </div><!-- /.product-image -->
		                                                    
		                                                
		                                                <div class="product-info text-left">
		                                                    <h3 class="name"><a href="detail.html">{{$product->name}}</a></h3>
		                                                    <div class="rating rateit-small"></div>
		                                                    <div class="description"></div>

		                                                    <div class="product-price"> 
		                                                        {{-- <span class="price">Rs {{round($product->price, 2)}} </span> --}}
		                                                        {{-- <span class="price-before-discount">$ 800</span> --}}
		                                                                            
		                                                    </div><!-- /.product-price -->
		                                                    
		                                                </div><!-- /.product-info -->
		                                                <div class="cart clearfix animate-effect">
		                                                    <div class="action">
		                                                        <ul class="list-unstyled">
		                                                            <li class="add-cart-button btn-group "  data-productid="{{$product->id}}" data-token="{{csrf_token()}}"type="button">
		                                                                <button data-toggle="tooltip" class="btn btn-primary icon product-modal" type="button" title="Enquiry" data-productname="{{$product->name}}" data-productId="{{$product->id}}">
		                                                                    <i class="fa fa-envelope-o"></i>                                                 
		                                                                </button>
		                                                                <button class="btn btn-primary cart-btn" 
		                                                               >Add to cart</button>   
		                                                            </li>
		                                                        </ul>
		                                                    </div><!-- /.action -->
		                                                </div><!-- /.cart -->
		                                            </div><!-- /.product -->
		                                      
		                                            </div><!-- /.products -->
		                                        </div><!-- /.item -->
		                                        @endforeach
		                                    </div><!-- /.home-owl-carousel -->
		                                </div><!-- /.product-slider -->
		                            </div><!-- /.tab-pane -->

		                        </div><!-- /.tab-content -->
		                    @endif

		                </div><!-- /.scroll-tabs -->
		                
		                    @endforeach
		                @endif   
		            </div>
		<!-- =========== SCROLL TABS : END ============ -->
			</div>
        </div><!-- /.row -->
            <!-- =========== BRANDS CAROUSEL ================ -->
                    {{-- @include('home.includes.brandslider') --}}
        <!-- ========= BRANDS CAROUSEL : END ================ -->
        </div><!-- /.container -->
    </div><!-- /#top-banner-and-menu --> 

    <!-- ========= Bootstrap Modal ================ -->
    <div class="modal fade" id="productmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
                  {{-- <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> --}}
                      <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                              <h4 style="color: green;" class="modal-title"></h4>
                            </div>
                            <div class="modal-body">
                                <form id="enquiry_form" data-parsley-validate=""    required="required" method="post" action="{{route('enquiry.submit')}}">
                                    {{csrf_field()}}
                                <label for="name">Name</label><br>
                                <input type="text" id= "name" name="fullname" placeholder="Your full name..." required /><br>
                                <!-- <input type="hidden" value="" id="productname" name="product_name"> -->
                                <label for="name">Email</label><br>
                                <input type="email" id= "email" name="email" placeholder="Your email..." required />
                                <br>
                                <label for="name">Quantity</label><br>
                                <input type="number" id="qty" name="quantity" placeholder="Quantity.." required />
                                <br>
                                <label for="name">Number</label><br>
                                <input type="text" id="num" data-minlength="20" name="contact" placeholder="Contact number" required />
                                <br>
                                <label for="name">Message</label><br>
                                <textarea name="enquiry" id="msg" placeholder="Your message..." data-minlength="20" required></textarea>
                                        
                                </form>
                            </div>
                            <div class="modal-footer">
                              <button id="sub" type="button" class="btn btn-default" onclick="submitEnquiryForm()" >Submit</button>
                            </div>
                    </div>
        </div>
    </div>
@stop

@section('pageScript')

<!-- Datatables -->
<script type="text/javascript" src="{{asset('packages/barryvdh/elfinder/js/standalonepopup.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets/home/js/cart.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/home/js/wishlist.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/home/js/compare.js') }}"></script>
<script type="text/javascript">
</script>

    
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script type="text/javascript">
    $(function() {
        $('.product-modal').on('click', function() {
            var productName = $(this).data('productname');
            var productId = $(this).data('productid');
            console.log(productName);
            console.log(productId);
            $('.modal-title').html(productName);
            $('#productname').val(productName);
            // $('.imagepreview').attr('src', $(this).find('img').attr('src'));
            $('#productmodal').modal('show');   
        });     
        });
    function submitEnquiryForm() {
       $("#enquiry_form").submit();
    }
    </script>
@endsection