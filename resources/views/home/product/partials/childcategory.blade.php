<div class="side-menu animate-dropdown outer-bottom-xs">
    <div class="head"><i class="icon fa fa-align-justify fa-fw"></i> {{ $parentname }}</div>        
    <nav class="yamm megamenu-horizontal" role="navigation">
    	<ul class="nav">
        @if(!empty($childCategory))
            @foreach($childCategory as $category)
    		<li class="dropdown menu-item">
    			<a href="{{route('category.product',['category'=>$parentname,'product'=>$category->name])}}" ><i class="icon fa fa-shopping-bag" aria-hidden="true"></i>{{$category->name}}</a>
    		</li><!-- /.menu-item -->
            @endforeach
        @endif
		</ul><!-- /.nav -->
	</nav><!-- /.megamenu-horizontal -->
</div><!-- /.side-menu -->