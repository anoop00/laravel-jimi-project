@extends('home.layouts.master') 
    @section('content')
         <div class="breadcrumb">
    <div class="container">
        <div class="breadcrumb-inner">
            <ul class="list-inline list-unstyled">
                <li><a href="#">Product</a></li>
                <li><a href="#"> Detail</a></li>
                <li class='active'>{{$product->name}}</li>
            </ul>
        </div><!-- /.breadcrumb-inner -->
    </div><!-- /.container -->
</div><!-- /.breadcrumb -->
<div class="body-content outer-top-xs">
    <div class='container'>
        <div class='row single-product'>
            <div class='col-md-12'>
                <div class="detail-block">
                    <div class="row  wow fadeInUp">
                    
                        <div class="col-xs-12 col-sm-6 col-md-5 gallery-holder">
                            <div class="product-item-holder size-big single-product-gallery small-gallery">

                                <div id="owl-single-product">
                                    <div class="single-product-gallery-item" id="slide1">
                                        <a data-lightbox="image-1" data-title="Gallery" href="{{ asset('assets/home/images/products/p8.jpg')}}">
                                            <img class="img-responsive" alt="" src="{{ asset($product->image)}}" data-echo="{{ asset($product->image)}}" />
                                        </a>
                                    </div><!-- /.single-product-gallery-item -->

                                    <div class="single-product-gallery-item" id="slide2">
                                        <a data-lightbox="image-1" data-title="Gallery" href="{{ asset('assets/home/images/products/p9.jpg')}}">
                                            <img class="img-responsive" alt="" src="{{ asset('assets/home/images/blank.gif')}}" data-echo="{{ asset('assets/home/images/products/p9.jpg')}}" />
                                        </a>
                                    </div><!-- /.single-product-gallery-item -->

                                    <div class="single-product-gallery-item" id="slide3">
                                        <a data-lightbox="image-1" data-title="Gallery" href="{{ asset('assets/home/images/products/p10.jpg')}}">
                                            <img class="img-responsive" alt="" src="{{ asset('assets/home/images/blank.gif')}}" data-echo="{{ asset('assets/home/images/products/p10.jpg')}}" />
                                        </a>
                                    </div><!-- /.single-product-gallery-item -->

                                    <div class="single-product-gallery-item" id="slide4">
                                        <a data-lightbox="image-1" data-title="Gallery" href="{{ asset('assets/home/images/products/p11.jpg')}}">
                                            <img class="img-responsive" alt="" src="{{ asset('assets/home/images/blank.gif')}}" data-echo="{{ asset('assets/home/images/products/p11.jpg')}}" />
                                        </a>
                                    </div><!-- /.single-product-gallery-item -->

                                    <div class="single-product-gallery-item" id="slide5">
                                        <a data-lightbox="image-1" data-title="Gallery" href="{{ asset('assets/home/images/products/p12.jpg')}}">
                                            <img class="img-responsive" alt="" src="{{ asset('assets/home/images/blank.gif')}}" data-echo="{{ asset('assets/home/images/products/p12.jpg')}}" />
                                        </a>
                                    </div><!-- /.single-product-gallery-item -->

                                    <div class="single-product-gallery-item" id="slide6">
                                        <a data-lightbox="image-1" data-title="Gallery" href="{{ asset('assets/home/images/products/p13.jpg')}}">
                                            <img class="img-responsive" alt="" src="{{ asset('assets/home/images/blank.gif')}}" data-echo="{{ asset('assets/home/images/products/p13.jpg')}}" />
                                        </a>
                                    </div><!-- /.single-product-gallery-item -->

                                    <div class="single-product-gallery-item" id="slide7">
                                        <a data-lightbox="image-1" data-title="Gallery" href="{{ asset('assets/home/images/products/p14.jpg')}}">
                                            <img class="img-responsive" alt="" src="{{ asset('assets/home/images/blank.gif')}}" data-echo="{{ asset('assets/home/images/products/p14.jpg')}}" />
                                        </a>
                                    </div><!-- /.single-product-gallery-item -->

                                    <div class="single-product-gallery-item" id="slide8">
                                        <a data-lightbox="image-1" data-title="Gallery" href="{{ asset('assets/home/images/products/p15.jpg')}}">
                                            <img class="img-responsive" alt="" src="{{ asset('assets/home/images/blank.gif')}}" data-echo="{{ asset('assets/home/images/products/p15.jpg')}}" />
                                        </a>
                                    </div><!-- /.single-product-gallery-item -->

                                    <div class="single-product-gallery-item" id="slide9">
                                        <a data-lightbox="image-1" data-title="Gallery" href="{{ asset('assets/home/images/products/p16.jpg')}}">
                                            <img class="img-responsive" alt="" src="{{ asset('assets/home/images/blank.gif')}}" data-echo="{{ asset('assets/home/images/products/p16.jpg')}}" />
                                        </a>
                                    </div><!-- /.single-product-gallery-item -->

                                </div><!-- /.single-product-slider -->

                            </div><!-- /.single-product-gallery -->
                        </div><!-- /.gallery-holder -->                 
                        <div class='col-sm-6 col-md-7 product-info-block'>
                            <div class="product-info">
                                <h1 class="name">{{ $product->name }}</h1>
                                
                                <div class="rating-reviews m-t-20">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="rating rateit-small"></div>
                                        </div>
                                        <div class="col-sm-8">
                                            {{-- <div class="reviews">
                                                <a href="#" class="lnk">( ({{count($reviews)}}) Reviews)</a>
                                            </div> --}}
                                        </div>
                                    </div><!-- /.row -->        
                                </div><!-- /.rating-reviews -->

                                <div class="stock-container info-container m-t-10">
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <div class="stock-box">
                                                <span class="label">Availability :</span>
                                            </div>  
                                        </div>
                                        <div class="col-sm-9">
                                            <div class="stock-box">
                                                <span class="value">In Stock</span>
                                            </div>  
                                        </div>
                                    </div><!-- /.row -->    
                                </div><!-- /.stock-container -->

                                <div class="description-container m-t-20">
                                    {{strip_tags($product->description)}}
                                </div><!-- /.description-container -->

                                <div class="price-container info-container m-t-20">
                                    <div class="row">
                                        

                                        <div class="col-sm-6">
                                            <div class="price-box">
                                                <span class="price"> </span>
                                                {{-- <span class="price-strike">$900.00</span> --}}
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="favorite-button m-t-10">
                                                @if(Auth::check())
                                                    <a class="btn btn-primary wish" data-toggle="tooltip" data-placement="right" title="Wishlist" href="#" data-productid="{{$product->product_id}}" data-token="{{csrf_token()}}" data-route="{{route('customer.addwishlist',$product->product_id)}}">
                                                        <i class="fa fa-heart"></i>
                                                    </a>
                                                @else
                                                    <a class="btn btn-primary emptywishlist" data-toggle="tooltip" data-placement="right" title="Wishlist" href="#" data-productid="{{$product->product_id}}" data-token="{{csrf_token()}}">
                                                        <i class="fa fa-heart"></i>
                                                    </a>
                                                @endif
                                                <a class="btn btn-primary compare" data-toggle="tooltip" data-placement="right" title="Add to Compare" href="#" data-productid="{{$product->product_id}}" data-token="{{csrf_token()}}">
                                                   <i class="fa fa-signal"></i>
                                                </a>
                                                <a class="btn btn-primary" data-toggle="tooltip" data-placement="right" title="E-mail" href="#">
                                                    <i class="fa fa-envelope"></i>
                                                </a>
                                            </div>
                                        </div>

                                    </div><!-- /.row -->
                                </div><!-- /.price-container -->

                                <div class="quantity-container info-container">
                                    <div class="row">
                                        
                                        {{-- <div class="col-sm-2">
                                            <span class="label">Qty :</span>
                                        </div>
                                        
                                        <div class="col-sm-2">
                                            <div class="cart-quantity">
                                                <div class="quant-input">
                                                    <div class="arrows">
                                                      <div class="arrow plus gradient"><span class="ir"><i class="icon fa fa-sort-asc"></i></span></div>
                                                      <div class="arrow minus gradient"><span class="ir"><i class="icon fa fa-sort-desc"></i></span></div>
                                                    </div>
                                                    <input type="text" value="1">
                                              </div>
                                            </div>
                                        </div> --}}

                                        <div class="col-sm-7">
                                            <a href="#" class="btn btn-primary product-modal"  data-productname="{{$product->name}}" data-productId="{{$product->id}}"><i class="fa fa-shopping-cart inner-right-vs"></i> Enquiry</a>
                                        </div>

                                        
                                    </div><!-- /.row -->
                                </div><!-- /.quantity-container -->
                            </div><!-- /.product-info -->
                        </div><!-- /.col-sm-7 -->
                    </div><!-- /.row -->
                </div>
                    
                <div class="product-tabs inner-bottom-xs  wow fadeInUp">
                    <div class="row">
                        <div class="col-sm-3">
                            <ul id="product-tabs" class="nav nav-tabs nav-tab-cell">
                                <li class="active"><a data-toggle="tab" href="#description">DESCRIPTION</a></li>
                            </ul><!-- /.nav-tabs #product-tabs -->
                        </div>
                        <div class="col-sm-9">

                            <div class="tab-content">
                                
                                <div id="description" class="tab-pane in active">
                                    <div class="product-tab">
                                        <p class="text">
                                    {{strip_tags($product->description)}}</p>
                                    </div>  
                                </div><!-- /.tab-pane -->

                                
                            </div><!-- /.tab-content -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.product-tabs -->

                    <!-- ============================================== UPSELL PRODUCTS ============================================== -->
                <!-- /.section -->
                <!-- ================ UPSELL PRODUCTS : END ================== -->
            
            </div><!-- /.col -->
            <div class="clearfix"></div>
        </div><!-- /.row -->
        
    </div><!-- /.container -->
</div><!-- /.body-content -->
    
    <!-- ========= Bootstrap Modal ================ -->
    <div class="modal fade" id="productmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
                  {{-- <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> --}}
                      <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                              <h4 style="color: green;" class="modal-title"></h4>
                            </div>
                            <div class="modal-body">
                                <form id="enquiry_form" data-parsley-validate=""    required="required" method="post" action="{{route('enquiry.submit')}}">
                                    {{csrf_field()}}
                                <label for="name">Name</label><br>
                                <input type="text" id= "name" name="fullname" placeholder="Your full name..." required /><br>
                                <!-- <input type="hidden" value="" id="productname" name="product_name"> -->
                                <label for="name">Email</label><br>
                                <input type="email" id= "email" name="email" placeholder="Your email..." required />
                                <br>
                                <label for="name">Quantity</label><br>
                                <input type="number" id="qty" name="quantity" placeholder="Quantity.." required />
                                <br>
                                <label for="name">Number</label><br>
                                <input type="text" id="num" data-minlength="20" name="contact" placeholder="Contact number" required />
                                <br>
                                <label for="name">Message</label><br>
                                <textarea name="enquiry" id="msg" placeholder="Your message..." data-minlength="20" required></textarea>
                                        
                                </form>
                            </div>
                            <div class="modal-footer">
                              <button id="sub" type="button" class="btn btn-default" onclick="submitEnquiryForm()" >Submit</button>
                            </div>
                    </div>
        </div>
    </div>
    @stop
@section('pageScript')
    <script type="text/javascript" src="{{ asset('assets/home/js/cart.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/home/js/wishlist.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/home/js/compare.js') }}"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script type="text/javascript">
    $(function() {
        $('.product-modal').on('click', function() {
            var productName = $(this).data('productname');
            var productId = $(this).data('productid');
            console.log(productName);
            console.log(productId);
            $('.modal-title').html(productName);
            $('#productname').val(productName);
            // $('.imagepreview').attr('src', $(this).find('img').attr('src'));
            $('#productmodal').modal('show');   
        });     
        });
    function submitEnquiryForm() {
       $("#enquiry_form").submit();
    }
    </script>
@endsection