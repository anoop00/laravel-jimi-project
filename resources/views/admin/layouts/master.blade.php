<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name') }} </title>

    <!-- Bootstrap -->
   
    <link href="{{ asset('assets/admin/vendors/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" >
    <link href="{{ asset('assets/admin/vendors/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" >
    <link href="{{ asset('assets/admin/vendors/nprogress/nprogress.css') }}" rel="stylesheet" type="text/css" >
    
    <link href="{{asset('assets/admin/build/css/iziToast.min.css')}}" rel="stylesheet">
    <script src="{{asset('assets/admin/js/iziToast.min.js')}}"></script>
    @if(Session::has('success'))
        <script type="text/javascript">
        iziToast.success({
            title: 'Success',
            message: '{{ Session::get('success') }}',
            position: 'topRight'
        });
        </script>
         @elseif(Session::has('warning'))
         <script type="text/javascript">
         iziToast.warning({
             title: 'Warning',
             message: '{{ Session::get('warning') }}',
             position: 'topRight'
         });
         </script>

    @endif
    <link href="{{ asset('assets/admin/vendors/iCheck/skins/flat/green.css') }}" rel="stylesheet" type="text/css" >
    <link href="{{ asset('assets/admin/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css') }}" rel="stylesheet" type="text/css" >
    <link href="{{ asset('assets/admin/vendors/jqvmap/dist/jqvmap.min.css') }}" rel="stylesheet" type="text/css" >
    <link href="{{ asset('assets/admin/build/css/custom.min.css') }}" rel="stylesheet" type="text/css" >
    @yield('pageCss')

    
  </head>
	<body class="nav-md">
		<div class="container body">
		    <div class="main_container">
			  	
                @include('admin.includes.asidebar')
                {{-- @include('admin.includes.sidebar') --}}
				@include('admin.includes.header')
				@yield('content')

				 
			</div>
		 	<footer>
			  	<div class="pull-right">
			      Designed by  <a href="http://www.khemrajregmi.com.np/" target="_blank">Khem Raj</a>
			  	</div>
			  	<div class="clearfix"></div>
			</footer>
	        <!-- /footer content -->
	     </div>

	    <!-- /gauge.js -->
	</body>
   <script type="text/javascript" src="{{ asset('assets/admin/vendors/jquery/dist/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/vendors/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/build/js/custom.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/js/moment/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js') }}"></script>
    <script src="{{ asset('assets/admin/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js')}}"></script>
<!-- validator -->
<script src="{{ asset('assets/admin/vendors/validator/validator.js')}}"></script>

<script type="text/javascript" src="{{asset('assets/admin/js/common_function.js')}}"></script>


<script>
    // initialize the validator function
    validator.message.date = 'not a real date';

    // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
    $('form')
            .on('blur', 'input[required], input.optional, select.required', validator.checkField)
            .on('change', 'select.required', validator.checkField)
            .on('keypress', 'input[required][pattern]', validator.keypress);

    $('.multi.required').on('keyup blur', 'input', function() {
        validator.checkField.apply($(this).siblings().last()[0]);
    });

    $('form').submit(function(e) {
        e.preventDefault();
        var submit = true;

        // evaluate the form using generic validaing
        if (!validator.checkAll($(this))) {
            submit = false;
        }

        if (submit)
            this.submit();

        return false;
    });
</script>
<!-- /validator -->
    @yield('pageScript')
   

</html>

