@extends('admin.layouts.master')
	@section('content')
    <!-- page content -->
          <div class="">
            <div class="row top_tiles">
              <div class="animated flipInY col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-caret-square-o-right"></i></div>
                  <div class="count" style="text-align: center;" >20</div>
                  <h3 style="text-align: center;">Total Categories</h3>
                  <p style="text-align: center;" >Lorem ipsum psdea itgum rixt.</p>
                </div>
              </div>

              
              <div class="animated flipInY col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-caret-square-o-right"></i></div>
                  <div class="count" style="text-align: center;" >20</div>
                  <h3 style="text-align: center;">Total Product </h3>
                  <p style="text-align: center;" >Lorem ipsum psdea itgum rixt.</p>
                </div>
              </div>

              
            </div>


            <div class="row top_tiles">
              <div class="animated flipInY col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-caret-square-o-right"></i></div>
                  <div class="count" style="text-align: center;" >20</div>
                  <h3 style="text-align: center;">Total Manufacturer</h3>
                  <p style="text-align: center;" >Lorem ipsum psdea itgum rixt.</p>
                </div>
              </div>

              
              <div class="animated flipInY col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-caret-square-o-right"></i></div>
                  <div class="count" style="text-align: center;" >20</div>
                  <h3 style="text-align: center;"></h3>
                  <p style="text-align: center;" >Lorem ipsum psdea itgum rixt.</p>
                </div>
              </div>

              
            </div>
            <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="dashboard_graph">

                <div class="row x_title">
                  <div class="col-md-6">
                    <h3>Network Activities <small>Graph title sub-title</small></h3>
                  </div>
                  <div class="col-md-6">
                    <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
                      <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                      <span>December 30, 2014 - January 28, 2015</span> <b class="caret"></b>
                    </div>
                  </div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12">
                  <div id="placeholder33" style="height: 260px; display: none" class="demo-placeholder"></div>
                  <div style="width: 100%;">
                    <div id="canvas_dahs" class="demo-placeholder" style="width: 100%; height:270px;"></div>
                  </div>
                </div>
                

                <div class="clearfix"></div>
              </div>
            </div>

          </div>            
          </div>
        <!-- /page content -->
  @endsection
  @section('pagescript')
    {{-- <script src="../vendors/nprogress/nprogress.js"></script>
    <script src="../vendors/Chart.js/dist/Chart.min.js"></script> --}}
  @endsection