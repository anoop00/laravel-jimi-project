    @extends('admin.layouts.master')
    @section('pageCss')
    <link href="{{ asset('assets/admin/js/colorbox/example2/colorbox.css') }}" rel="stylesheet" type="text/css" />
    @endsection('pageCss')
    @section('content')
    <div class="clearfix"></div>
    <div class="row">
     @foreach ($errors->all() as $error)
     <p class="error">{{ $error }}</p>
     @endforeach

     <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Setting <small>Edit Site Settings</small></h2>
          <div class="clearfix"></div>
        </div>
        <div >
          <form id="myForm" data-parsley-validate="" class="form-horizontal form-label-left"
          required="required" method="post" action="{{route('setting.update','config')}}">
          <input type="hidden" name="_method" value="PUT">
          {{csrf_field()}}
          <div id="wizard" class="form_wizard wizard_horizontal">
            <ul class="wizard_steps">
              <li>
                <a href="#step-1">
                  <span class="step_no">1</span>
                  <span class="step_descr">
                    Step 1<br/>
                    <small>General</small>
                  </span>
                </a>
              </li>
              <li>
                <a href="#step-2">
                  <span class="step_no">2</span>
                  <span class="step_descr">
                    Step 2<br/>
                    <small>Data</small>
                  </span>
                </a>
              </li>
            </ul>
            <div id="step-1">
            
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="store-name">Site
                    Name<span class="required">*</span>
                  </label>
                  <div class="col-md-7 col-sm-6 col-xs-12">
                    <input type="text" name="config_name" id="config_name"
                    value="{{$setting['config_name']}}"
                    class="form-control col-md-7 col-xs-12" placeholder="Site Name">
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">
                    Logo
                  </label>
                  <div class="col-md-7 col-sm-6 col-xs-12">
                    <input type="text" id="feature_image" name="config_logo"
                    class="form-control col-md-7 col-xs-12"
                    value="{{$setting['config_logo']}}" placeholder="Site Logo">
                    <a href="" class="popup_selector" data-inputid="feature_image">Browse
                      Image</a>
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="address">Address
                    <span class="required"></span>
                  </label>
                  <div class="col-md-7 col-sm-6 col-xs-12">
                    <textarea rows="4" cols="50" name="config_address" id='meta-tag-description'
                    class="form-control col-md-7 col-xs-12"
                    placeholder="Address">{{$setting['config_address']}}</textarea>
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Email">Email<span
                    class="required">*</span>
                  </label>
                  <div class="col-md-7 col-sm-6 col-xs-12">
                    <input type="text" name="config_email" id="config_email"
                    value="{{$setting['config_email']}}"
                    class="form-control col-md-7 col-xs-12" placeholder="Email">
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="telephone">Telephone<span
                    class="required">*</span>
                  </label>
                  <div class="col-md-7 col-sm-6 col-xs-12">
                    <input type="text" name="config_telephone" id="config_telephone"
                    value="{{$setting['config_telephone']}}"
                    class="form-control col-md-7 col-xs-12" placeholder="Telephone">
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Fax">Fax
                  </label>
                  <div class="col-md-7 col-sm-6 col-xs-12">
                    <input type="text" name="config_fax" id="config_fax"
                    value="{{$setting['config_fax']}}" class="form-control col-md-7 col-xs-12"
                    placeholder="Fax">
                  </div>
                </div>

            </div>
            <div id="step-1">
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="meta-tag-title">Meta
                    Title <span class="required">*</span>
                  </label>
                  <div class="col-md-7 col-sm-6 col-xs-12">
                    <input type="text" name="config_meta_title" id="first-name"
                    value="{{$setting['config_meta_title']}}"
                    class="form-control col-md-7 col-xs-12" placeholder="Meta Title">
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="meta-tag-description">Meta
                    Tag Description <span class="required"></span>
                  </label>
                  <div class="col-md-7 col-sm-6 col-xs-12">
                    <textarea rows="4" cols="50" name="config_meta_description"
                    id='meta-tag-description' class="form-control col-md-7 col-xs-12"
                    placeholder="Meta Tag Description">{{$setting['config_meta_description']}}</textarea>
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="meta-keywords">Meta
                    Tag Keyword <span class="required"></span>
                  </label>
                  <div class="col-md-7 col-sm-6 col-xs-12">
                    <input type="text" name="config_meta_keyword" id="first-name"
                    value="{{$setting['config_meta_keyword']}}"
                    class="form-control col-md-7 col-xs-12" placeholder="Meta Tag Keyword">
                  </div>
                </div>

            </div>

                
              
    </form>
    </div>


    </div>
    <!-- End SmartWizard Content -->


    </div>
    </div>
    </div>
    </div>
    @endsection

    @section('pageScript')
    <script type="text/javascript" src="{{ asset('assets/admin/js/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('assets/admin/js/colorbox/jquery.colorbox-min.js') }}"></script>
    <script type="text/javascript" src="{{asset('packages/barryvdh/elfinder/js/standalonepopup.min.js')}}"></script>

    <script src="{{asset('assets/admin/vendors/iCheck/icheck.min.js')}}"></script>
    {{-- <script src="{{ asset('assets/admin/js/colorbox/jquery.colorbox-min.js') }}"></script> --}}

    <script src="{{asset('assets/admin/vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js')}}"></script>
    <script src="{{asset('assets/admin/vendors/jquery.hotkeys/jquery.hotkeys.js')}}"></script>
    <script src="{{asset('assets/admin/vendors/google-code-prettify/src/prettify.js')}}"></script>

    <script type="text/javascript">
     CKEDITOR.replace( 'messageArea',
     {
      customConfig : 'config.js',
      toolbar : 'simple'
    })
    </script> 
    <script>
      $(document).ready(function() {
        // alert('hello');

        $('#wizard').smartWizard();

        $('#wizard_verticle').smartWizard({
          transitionEffect: 'slide'
        });

        $('.buttonNext').addClass('btn btn-success');
        $('.buttonPrevious').addClass('btn btn-primary');
        $('.buttonFinish').addClass('btn btn-default');
        $(".buttonFinish").click(function(){
          $('#myForm').submit();
        });
      });
    </script>




  @endsection