@extends('admin.layouts.master')
@section('pageCss')
         <!-- Datatables -->
<link href="{{asset('assets/admin/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
<link href="{{asset('assets/admin/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css')}}" rel="stylesheet">
<link href="{{asset('assets/admin/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet')}}">
<link href="{{asset('assets/admin/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css')}}" rel="stylesheet">
<link href="{{asset('assets/admin/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css')}}" rel="stylesheet">

<link rel="stylesheet" href="{{ asset('home/css/iziToast.min.css')}}">
    
<script type="text/javascript" src="{{ asset('home/js/iziToast.min.js')}}"></script>
<!-- Datatables -->
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
                    @if(Session::has('success'))
                        <script type="text/javascript">
                        iziToast.success({
                            title: 'Success',
                            message: '{{ Session::get('success') }}',
                            position: 'topRight'
                                    });
                        </script>
                    @endif
                    <div class="x_title">
                    <h2>Pages <small> List</small></h2>
                    <div class="pull-right">
                        <a href="{{route('page.create')}}" class="btn btn-info btn-md">
                            <span class="glyphicon glyphicon-plus"></span> Add
                        </a>
                        <a href="#" class="btn btn-danger btn-md deleteData">
                            <span class="glyphicon glyphicon-trash" ></span> Delete
                        </a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">

                  <table id="datatable-checkbox" class="table table-striped table-bordered bulk_action">
                        <thead>
                        <tr>
                            <th><input type="checkbox" id="check-all" class="flat"></th>
                            <th>Page Image</th>
                            <th>Page Name</th>
                            <th>Page Title</th>
                            <th>Slug</th>
                            <th>Category</th>
                            <th>Status</th>
                            <th>Action</th>

                        </tr>
                        </thead>


                        <tbody>
                        @foreach($pages as $page)
                        <tr> 
                            <td><input type="checkbox" class="flat" value="{{$page->page_id}}" name="table_records"></td>
                            <td>
                                @if($page->image!= '')
                                    <img src=" {{ asset($page->image )}} " style="height: 60px;" alt="Product Name Missmatch" >
                                @else
                                    <img  src="{{asset('')}}admin/images/deafaul-product-img.png" alt="..." style="height: 60px;">
                                @endif
                            </td>
                            <td>{{$page->name}}</td>
                            <td>{{$page->title}}</td>
                            <td>{{$page->slug}}</td>
                            <td>
                                {{$page->name}}
                            </td>
                            <td><?php
                                    if ($page->status==0) 
                                        echo "Disable";
                                    else
                                        echo "Enable";
                                ?>
                            </td>
                            <td>
                                {{-- <a href="{{route('page.show',$page->page_id)}}" data-toggle="tooltip" title="View Detail" class="btn btn-info"><i class="fa fa-eye"></i></a> --}}
                                <a href="{{route('page.edit',$page->page_id)}}" class="btn btn-default btn-sm"><i class="fa fa-edit"></i> Edit
                                </a>
                            </td>
                        </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
    </div>
@endsection('content')
@section('pageScript')

<!-- Datatables -->
<script src="{{asset('assets/admin/vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/admin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('assets/admin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('assets/admin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js')}}"></script>
<script src="{{asset('assets/admin/vendors/datatables.net-buttons/js/buttons.flash.min.js')}}"></script>
<script src="{{asset('assets/admin/vendors/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('assets/admin/vendors/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
<script src="{{asset('assets/admin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js')}}"></script>
<script src="{{asset('assets/admin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js')}}"></script>
<script src="{{asset('assets/admin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('assets/admin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js')}}"></script>
<script src="{{asset('assets/admin/vendors/datatables.net-scroller/js/dataTables.scroller.min.js')}}"></script>
<!-- Datatables -->
<!-- iCheck -->
<script src="{{asset('assets/admin/vendors/iCheck/icheck.min.js')}}"></script>

<!-- Datatables -->
    <script>
        $(document).ready(function() {


            $('#datatable').dataTable();

            $('#datatable-keytable').DataTable({
                keys: true
            });

            $('#datatable-responsive').DataTable();

            $('#datatable-fixed-header').DataTable({
                fixedHeader: true
            });

            var $datatable = $('#datatable-checkbox');

            $datatable.dataTable({
                'order': [[ 1, 'asc' ]],
                'columnDefs': [
                    { orderable: false, targets: [0] }
                ]
            });
            $datatable.on('draw.dt', function() {
                $('input').iCheck({
                    checkboxClass: 'icheckbox_flat-green'
                });
            });

        });
    </script>
    <!-- /Datatables -->

    <script type="text/javascript">
        var deleteRow = $('.deleteData');
        var route = "page";
        var token = "{{csrf_token()}}";
        deleteTableRow(deleteRow,route,token);
    </script>
@endsection('pageScript')