<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar naadmin.dashboard.indexv_title" style="border: 0;">
                    {{-- @if($setting['config_logo']!='')
                        <a href="{{route('dashboard.index')}}" class="site_title"><img  src="{{ asset($setting['config_logo']) }}" alt="" style=""></a>
                    @else
                        <a href="{{route('dashboard.index')}}" class="site_title"><img  src="{{asset('')}}assets/home/images/suvalaav-logo.svg" alt="" style=""></a>
                    @endif --}}


                </div>
                <div class="clearfix"></div>
                <!-- menu profile quick info -->
                <div class="profile">
                    {{-- @if(auth()->user()->image!=='')
                        <div class="profile_pic">
                            <img src="{{ asset(Sentinel::getUser()->image)}}" alt="{{ Sentinel::getUser()->image }}" class="img-circle profile_img">
                        </div>
                    @else
                        <div class="profile_pic">
                            <img  src="{{asset('')}}assets/admin/images/defaultimage.png" alt="" class="img-circle profile_img">
                        </div>
                    @endif
                    
                    <div class="profile_info">
                        <span>Welcome,</span>
                        <h2>{{Sentinel::getUser()->first_name}} {{Sentinel::getUser()->last_name}}</h2>
                    </div> --}}
                </div>
                <!-- /menu profile quick info -->
                <br/>
                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section">
                        <ul class="nav side-menu">
                        </ul>

                        <ul class="nav side-menu">
                            <li><a href="{{route('dashboard.index')}}"><i class="fa fa-dashboard fw"></i> Dashboard </a>
                            </li>
                        </ul>
                        <ul class="nav side-menu">
                            <li><a><i class="fa fa-tags fw"></i> Catalog <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{route('category.index')}}">Categories</a></li>
                                    <li><a href="{{route('product.index')}}">Products</a></li>
                                    <li><a href="{{route('manufacturer.index')}}">Manufacturers</a></li>
                                    {{-- <li><a href="{{route('review.index')}}">Reviews</a></li> --}}
                                    {{-- <li><a href="{{route('information.index')}}">Information</a></li> --}}
                                </ul>
                            </li>
                        </ul>
                        <ul class="nav side-menu">
                            <li><a><i class="fa fa-shopping-cart fw"></i> Store <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{route('store.index')}}">Stores</a></li>
                                </ul>
                            </li>
                        </ul>
                        <ul class="nav side-menu">
                            <li><a><i class="fa fa-television fw"></i> Design <span
                                            class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{route('banner.index')}}">Banners</a></li>
                                </ul>
                            </li>
                        </ul>
                        
                        
                        
                       
                        <ul class="nav side-menu">
                            <li><a><i class="fa fa-cog fw"></i> System <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{route('setting.index')}}">Site Settings </a></li>
                                </ul>
                            </li>
                        </ul>
                        
                        
                    </div>
                </div>
                <!-- /sidebar menu -->
                <!-- /menu footer buttons -->
                <div class="sidebar-footer hidden-small">
                    <a data-toggle="tooltip" data-placement="top" title="Settings">
                        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                        <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Lock">
                        <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                    </a>
                    <a href="{{route('logout')}}" data-toggle="tooltip" data-placement="top" title="Logout">
                        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                    </a>
                </div>
                <!-- /menu footer buttons -->
            </div>
        </div>