<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar naadmin.dashboard.indexv_title" style="border: 0;">
                    {{-- @if($setting['config_logo']!='') --}}
                        {{-- <a href="{{route('dashboard.index')}}" class="site_title"><img  src="{{ asset($setting['config_logo']) }}" alt="" style=""></a>
                    @else --}}
                        <a href="{{route('admin.dashboard')}}" class="site_title"><img  src="{{asset('')}}assets/home/images/suvalaav-logo.svg" alt="" style=""></a>
                    {{-- @endif --}}


                </div>
                <div class="clearfix"></div>
                <!-- menu profile quick info -->
                <div class="profile">
                        <div class="profile_pic">
                            <img  src="{{asset('')}}assets/admin/images/defaultimage.png" alt="" class="img-circle profile_img">
                        </div>

                    <div class="profile_info">
                        <span>Welcome,</span>
                        @if(Auth::user() != null)
                        <h2>{{Auth::user()->name}}</h2>
                        @else
                        <h2>Admin</h2>
                        @endif
                    </div>
                </div>
                <!-- /menu profile quick info -->
                <br/>
                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section">
                        <ul class="nav side-menu">
                        </ul>

                        <ul class="nav side-menu">
                            <li><a href="{{route('admin.dashboard')}}"><i class="fa fa-dashboard fw"></i> Dashboard </a>
                            </li>
                        </ul>
                        <ul class="nav side-menu">
                            <li><a><i class="fa fa-tags fw"></i> Catalog <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{route('category.index')}}">Categories</a></li>
                                    <li><a href="{{route('product.index')}}">Products</a></li>
                                    <li><a href="{{route('manufacturer.index')}}">Manufacturers</a></li>
                                </ul>
                            </li>
                        </ul>
                        <ul class="nav side-menu">
                            <li><a><i class="fa fa-television fw"></i> Design <span
                                            class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{route('banner.index')}}">Banners</a></li>
                                </ul>
                            </li>
                        </ul>
                        {{-- <ul class="nav side-menu">
                            <li><a><i class="fa fa-user fw"></i> Customers <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{route('customer.index')}}">Customers</a></li>
                                    <li><a href="{{route('customergroup.index')}}">Customer Groups</a></li>
                                    <li><a href="{{route('customerfamilysize.index')}}">Customer Family Size</a></li>
                                    <li><a href="{{route('duration.index')}}">Customer FamilyWishlist Duration</a></li>
                                    <li><a href="{{route('customerproductsuggestion.index')}}">Customer Product Suggestion</a></li>
                                </ul>
                            </li>
                        </ul> --}}
                        {{-- <ul class="nav side-menu">
                            <li><a><i class="fa fa-share-alt fw"></i> Marketing <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{route('marketing.index')}}">Marketing</a></li>
                                    <li><a href="{{route('mail.index')}}">Mail</a></li>
                                    <li><a href="{{route('offer.index')}}">Offer</a></li>
                                    <li><a href="{{route('coupon.index')}}">Coupons</a></li>
                                    <li><a>Discounts<span class="fa fa-chevron-down"></span> </a>
                                        <ul class="nav child_menu">
                                            <li><a href="{{route('discounttype.index')}}">Discount Type </a>
                                            <li><a href="{{route('discount.index')}}">Discounts</a></li>
                                        </ul>
                                    </li>

                                    <li><a>Combo Offers<span class="fa fa-chevron-down"></span> </a>
                                        <ul class="nav child_menu">
                                            <li><a href="{{route('combotype.index')}}">Combo Offer Type </a>
                                            <li><a href="{{route('combooffer.index')}}">Combo Offers</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                        </ul> --}}
                       {{-- @if(Sentinel::hasAccess('admin')) --}}
                        <ul class="nav side-menu">
                            <li><a><i class="fa fa-cog fw"></i> System <span class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><a href="{{route('setting.index')}}">Site Settings </a></li>
                                    {{-- <li><a>Localization <span class="fa fa-chevron-down"></span> </a>
                                        <ul class="nav child_menu">
                                            <li><a href="{{route('orderstatus.index')}}">Order Statuses </a></li>
                                            <li><a href="{{route('stockstatus.index')}}">Stock Statuses </a></li>
                                            <li><a href="{{route('lengthclass.index')}}">LengthClasses</a></li>
                                            <li><a href="{{route('weightclass.index')}}">WeightClasses</a></li>
                                            <li><a>Returns <span class="fa fa-chevron-down"></span> </a>
                                                <ul class="nav child_menu">
                                                    <li><a href="{{route('returnstatus.index')}}">Return Statuses</a>
                                                    <li><a href="{{route('returnaction.index')}}">Returns Actions</a></li>
                                                    <li><a href="{{route('returnreason.index')}}">Returns Reasons</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li> --}}
                                   {{--  <li><a>Users <span class="fa fa-chevron-down"></span> </a>
                                        <ul class="nav child_menu">
                                            <li><a href="{{route('user.index')}}">Users </a></li>
                                            <li><a href="{{route('role.index')}}">User Roles </a></li>
                                        </ul>
                                    </li> --}}
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- /sidebar menu -->
                <!-- /menu footer buttons -->
                <div class="sidebar-footer hidden-small">
                    <a data-toggle="tooltip" data-placement="top" title="Settings">
                        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                        <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Lock">
                        <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                    </a>
                    <a href="{{route('logout')}}" data-toggle="tooltip" data-placement="top" title="Logout">
                        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                    </a>
                </div>
                <!-- /menu footer buttons -->
            </div>
        </div>