<div class="x_title">
    <h2><?php echo $title; ?> <small> List</small></h2>
    <div class="pull-right">
	    @if(($name=='category'))
	    	<a href="{{route('admin.'.$name.'.importexport')}}" class="btn btn-info btn-md">Import/Export
	    	</a>
	    @endif
	    <a href="{{route('admin.'.$name.'.create')}}" class="btn btn-info btn-md">
	        <span class="glyphicon glyphicon-plus"></span> Add
	    </a>
	    <a href="#" class="btn btn-danger btn-md deleteData">
	        <span class="glyphicon glyphicon-trash" ></span> Delete
	    </a>
    </div>
    <div class="clearfix"></div>
</div>