@if (Session::has('notifier.notice'))
    <script>
        new PNotify({!! Session::get('notifier.notice') !!});
    </script>
@endif
@if(Session::has('message'))
    <div class="alert {{ Session::get('alert-class', 'alert-info') }} fade in" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <p>{{ Session::get('message') }}</p>
    </div>
    {{ Session::forget('message') }}
@endif