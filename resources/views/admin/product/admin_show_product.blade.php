@extends('admin.layouts.master')
@section('pageCss')
<link href="{{ asset('assets/admin/js/colorbox/example2/colorbox.css') }}" rel="stylesheet" type="text/css" />
<style>
  .form_wizard .stepContainer {
    height: auto !important;
  }
  .youtube-image{
    padding-top: 150px;
  }
  .youtube-image iframe{
    width: 100%;
  }
</style>
@endsection('pageCss')
@section('content')
<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel ">
      <div class="x_title">
          <h2>Product <small>Show Product</small></h2>
          <div class="pull-right">
               <button type="reset" class="btn btn-primary" onclick="history.go(-1);">Back</button>
            </div>
            <div class="clearfix"></div>
          <div class="clearfix"></div>
      </div>
      {{-- {{ dd($product) }} --}}
      {{-- {!! <iframe width="560" height="315" src="https://www.youtube.com/embed/qeFdEhtIkk0" frameborder="0" allowfullscreen></iframe> !!} --}}

      <div class="col-md-8">
        <form id="myForm" data-parsley-validate="" class="form-horizontal form-label-left" >
          <input type="hidden" name="_method" value="PUT">
          {{csrf_field()}}

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Product Name <span class="required">:</span>
            </label>
            <div class="col-md-7 col-sm-6 col-xs-12">
              {{$product->name}}
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Product Description <span class="required">:</span>
            </label>
            <div class="col-md-7 col-sm-6 col-xs-12">{{strip_tags($product->description)}}
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Meta Title <span class="required">:</span>
            </label>
            <div class="col-md-7 col-sm-6 col-xs-12">
             {{$product->meta_title}}
           </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Meta Keywords<span class="required">:</span>
            </label>
            <div class="col-md-7 col-sm-6 col-xs-12">
              {{$product->meta_keywords}}
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Product Tag<span class="required">:</span>
            </label>
            <div class="col-md-7 col-sm-6 col-xs-12">
             {{$product->tag}}
           </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Product SKU<span class="required">:</span>
            </label>
            <div class="col-md-7 col-sm-6 col-xs-12">
             {{$product->sku}}
           </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Model <span class="required">:</span>
            </label>
            <div class="col-md-7 col-sm-6 col-xs-12">
             {{$product->model}}
           </div>
          </div>


          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Quantity in Stock <span class="required">:</span>
            </label>
            <div class="col-md-7 col-sm-6 col-xs-12">{{$product->quantity}}
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Price/Quantity<span class="required">:</span>
            </label>
            <div class="col-md-7 col-sm-6 col-xs-12">{{$product->price}}
            </div>
          </div>


          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Manufacturer<span class="required">:</span>
            </label>
            <div class="col-md-7 col-sm-6 col-xs-12">{{$product->manufacturer->name}}
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Weight Class<span class="required">:</span>
            </label>
            <div class="col-md-7 col-sm-6 col-xs-12">{{$product->weightClass->title}}
            </div>
          </div>


          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Length Class<span class="required">:</span>
            </label>
            <div class="col-md-7 col-sm-6 col-xs-12">{{$product->lengthClass->title}}
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Height Class<span class="required">:</span>
            </label>
            <div class="col-md-7 col-sm-6 col-xs-12"><?php
              if ($product->status==0)
                echo "Disable";
              else
                echo "Enable";
              ?>
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Stock Status<span class="required">:</span>
            </label>
            <div class="col-md-7 col-sm-6 col-xs-12">{{$product->stockStatus->name}}
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Total View<span class="required">:</span>
            </label>
            <div class="col-md-7 col-sm-6 col-xs-12">{{$product->viewed}}
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">New Arrival<span class="required">:</span>
            </label>
            <div class="col-md-7 col-sm-6 col-xs-12">
              <?php
              if ($product->newarrival==1)
                echo "Yes";
              else
                echo "No";
              ?>
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">SKU<span class="required">:</span>
            </label>
            <div class="col-md-7 col-sm-6 col-xs-12">
              {{ $product->sku }}
            </div>
          </div>

          {{--  <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Shipping<span class="required">:</span>
            </label>
            <div class="col-md-7 col-sm-6 col-xs-12">
              {{ $product->shipping }}
            </div>
          </div> --}}



          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Dimension (L x W x H)<span class="required">:</span>
            </label>
            <div class="col-md-7 col-sm-6 col-xs-12">
              {{ round($product->length, 2) }} X {{ round($product->width, 2 )}} X {{round($product->height, 2)}}
            </div>
          </div>
          

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Substract Stock<span class="required">:</span>
            </label>
            <div class="col-md-7 col-sm-6 col-xs-12"><?php
              if ($product->status==1)
                echo "Yes";
              else
                echo "No";
              ?>
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Minimum <span class="required">:</span>
            </label>
              <div class="col-md-7 col-sm-6 col-xs-12">
                {{ $product->minimum}}
              </div>
          </div>

          

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Discountable Product<span class="required">:</span>
            </label>
            <div class="col-md-7 col-sm-6 col-xs-12"><?php
              if ($product->is_discountable==1)
                echo "Yes";
              else
                echo "No";
              ?>
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Status<span class="required">:</span>
            </label>
            <div class="col-md-7 col-sm-6 col-xs-12"><?php
              if ($product->status==0)
                echo "Disable";
              else
                echo "Enable";
              ?>
            </div>
          </div>


          <!-- <div class="ln_solid"></div> -->
          <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
              
            </div>
          </div>
        
        </form> 
      </div>

      <div class="col-md-4">
        <div>
            @if(($product->image)!='')
                <img src="{{asset($product->image)}}" alt="" />
            @else
                <img  src="{{asset('')}}assets/admin/images/deafaul-product-img.png" alt="...">
            @endif
        </div>

        <div class="youtube-image">
          {{-- {!! ($product->video_link) !!} --}}
            @if(($product->video_link)!='')
                {!! ($product->video_link) !!}
            @else
                <iframe width="560" height="315" src="https://www.youtube.com/embed/qeFdEhtIkk0" frameborder="0" allowfullscreen></iframe>
            @endif
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('pageScript')
<script src="{{asset('assets/admin/vendors/iCheck/icheck.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('assets/admin/js/ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('assets/admin/js/colorbox/jquery.colorbox-min.js') }}"></script>
<script type="text/javascript" src="{{asset('packages/barryvdh/elfinder/js/standalonepopup.min.js')}}"></script>
<script src="{{asset('assets/admin/vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js')}}"></script>
<script src="{{asset('assets/admin/vendors/jquery.hotkeys/jquery.hotkeys.js')}}"></script>
<script src="{{asset('assets/admin/vendors/google-code-prettify/src/prettify.js')}}"></script>

<!-- <script type="text/javascript">
 CKEDITOR.replace( 'messageArea',
 {
  customConfig : 'config.js',
  toolbar : 'simple'
})
</script>  -->
@endsection
