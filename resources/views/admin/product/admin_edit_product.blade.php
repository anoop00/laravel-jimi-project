@extends('admin.layouts.master')
@section('pageCss')
    <link href="{{ asset('assets/admin/js/colorbox/example2/colorbox.css') }}" rel="stylesheet" type="text/css"/>
    <!-- Select2 -->
    <link href="{{asset('assets/admin/vendors/select2/dist/css/select2.min.css')}}" rel="stylesheet">
    <style>
        .form_wizard .stepContainer {
            height: 600px !important;
        }
    </style>
@endsection
@section('content')
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2><i class="fa fa-pencil"></i> Product
                        <small>Edit Product</small>
                    </h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br>
                    @if (count($errors) > 0)
                        <div class="alert alert-danger alert-dismissible fade in" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                        aria-hidden="true">×</span>
                            </button>
                            <strong>OOPS! You might have missed to fill some required fields. Please check the errors.
                                <strong>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                        </div>
                    @endif
                    <form id="demo-form2" data-parsley-validate="" class="form-horizontal form-label-left"
                          action="{{route('product.update',$product->id)}}" method="post">
                        <input type="hidden" name="_method" value="PUT">
                        {{csrf_field()}}
                        <div id="wizard" class="form_wizard wizard_horizontal">
                            <ul class="wizard_steps">
                                <li>
                                    <a href="#step-1">
                                        <span class="step_no">1</span>
                            <span class="step_descr">
                                              Step 1<br/>
                                              <small>General</small>
                                          </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#step-2">
                                        <span class="step_no">2</span>
                            <span class="step_descr">
                                              Step 2<br/>
                                              <small>Data</small>
                                          </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#step-3">
                                        <span class="step_no">3</span>
                            <span class="step_descr">
                                              Step 3<br/>
                                              <small>Links</small>
                                          </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#step-4">
                                        <span class="step_no">6</span>
                                            <span class="step_descr">
                                              Step 6<br/>
                                              <small>Image</small>
                                            </span>
                                    </a>
                                </li>
                            </ul>
                            <div id="step-1">
                                <form class="form-horizontal form-label-left">
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Product Name
                                            <span class="required">*</span>
                                        </label>
                                        <div class="col-md-7 col-sm-6 col-xs-12">
                                            <input type="text" id="name" placeholder="Product Name" name="name"
                                                   value="{{$product->name}}" class="form-control col-md-7 col-xs-12">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Description
                                            <span class="required"></span>
                                        </label>
                                        <div class="col-md-7 col-sm-6 col-xs-12">
                                            <textarea id="messageArea" name="description" rows="7"
                                                      class="form-control ckeditor"
                                                      placeholder="Write your message..">{{$product->description}}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="meta_tag">Meta Tag
                                            Title <span class="required">*</span>
                                        </label>
                                        <div class="col-md-7 col-sm-6 col-xs-12">
                                            <input type="text" id="meta_title" name="meta_title"
                                                   placeholder="Meta Tag Title" value="{{$product->meta_title}}"
                                                   class="form-control col-md-7 col-xs-12">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12"
                                               for="meta_tag_description">Meta
                                            Tag Description
                                        </label>
                                        <div class="col-md-7 col-sm-6 col-xs-12">
                                            <textarea class="resizable_textarea form-control" name="meta_description"
                                                      placeholder="Meta Tag Description">{{$product->meta_description}}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12"
                                               for="meta_tag_keywords">Meta Tag Keywords
                                        </label>
                                        <div class="col-md-7 col-sm-6 col-xs-12">
                                            <textarea class="resizable_textarea form-control" name="meta_keywords"
                                                      placeholder="Meta Tag Keywords">{{$product->meta_keywords}}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Product Tags">Product
                                            Tags
                                        </label>
                                        <div class="col-md-7 col-sm-6 col-xs-12">
                                            <input type="text" id="tag" name="tag" placeholder="Product Tags"
                                                   value="{{$product->tag}}" class="form-control col-md-7 col-xs-12">
                                        </div>
                                    </div>
                            </div>
                            <div id="step-2">
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="model">Model<span
                                                class="required">*</span>
                                    </label>
                                    <div class="col-md-7 col-sm-6 col-xs-12">
                                        <input type="text" id="name" name="model" placeholder="Model"
                                               value="{{$product->model}}" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="sku">SKU
                                    </label>
                                    <div class="col-md-7 col-sm-6 col-xs-12">
                                        <input type="text" id="sku" name="sku" value="{{$product->sku}}"
                                               placeholder="SKU" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="price">Price<span class="required">*</span>
                                    </label>
                                    <div class="col-md-7 col-sm-6 col-xs-12">
                                        <input type="number" id="price" name="price" value="{{$product->price}}"
                                               placeholder="Price" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="quantity">Quantity
                                    </label>
                                    <div class="col-md-7 col-sm-6 col-xs-12">
                                        <input type="text" id="quantity" name="quantity" placeholder="Quantity"
                                               value="{{$product->quantity}}" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="minimum_quantity">Minimum
                                        Quantity
                                    </label>
                                    <div class="col-md-7 col-sm-6 col-xs-12">
                                        <input type="number" id="minimum" name="minimum" placeholder="Minimum Quantity"
                                               value="{{$product->minimum}}" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>
                                
                                
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="weight">Wight
                                    </label>
                                    <div class="col-md-7 col-sm-6 col-xs-12">
                                        <input type="number" id="weight" name="weight" placeholder="Weight"
                                               value="{{$product->weight}}" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Status</label>
                                    <div class="col-md-7 col-sm-6 col-xs-12">
                                        <select class="form-control" name="status">
                                            <option value="1" <?php echo ($product->status == 1) ? 'selected' : ''; ?>>
                                                Enabled
                                            </option>
                                            <option value="0" <?php echo ($product->status == 0) ? 'selected' : ''; ?>>
                                                Disabled
                                            </option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div id="step-3">
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12"
                                           for="model">Manufacturer <span
                                                class="required">*</span>
                                    </label>
                                    <div class="col-md-7 col-sm-6 col-xs-12">
                                        <select class="form-control col-md-7 col-xs-12" name="manufacturer_id">
                                            @foreach($manufacturers as $m)
                                                <option value="{{$m->id}}" <?php echo ($product->manufacturer_id == $m->id) ? 'selected' : ''; ?>>{{$m->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="sku">Catogories<span class="required">*</span>
                                    </label>
                                    <div class="col-md-7 col-sm-6 col-xs-12">
                                        <select class="select2_multiple form-control" multiple="multiple" name="category_id">
                                            @foreach($categories as $c)
                                              @foreach ($c->children as $children)
                                              <option value="{{$children->id}}" <?php echo ($children->id == $product->category->first()->id) ? 'selected' : ''; ?>>{{$children->name}}</option>
                                              @endforeach
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-sm-3 col-xs-12 control-label">
                                       New Arrival
                                    </label>
                                    <div class="radio">
                                        <label>
                                            <input type="radio" value="1" class="flat" checked name="newarrival" <?php echo ($product->newarrival == 1) ? 'checked' : ''; ?>> Yes
                                        </label>
                                        <label>
                                            <input type="radio" value="0" class="flat"  name="newarrival" <?php echo ($product->newarrival == 0) ? 'checked' : ''; ?>> No
                                        </label>
                                       
                                    </div>
                                </div>
                            </div>
                            <div id="step-4">
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Image<span
                                                class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="feature_image" name="image"
                                               class="form-control col-md-7 col-xs-12" value="{{$product->image}}"
                                               placeholder="Product Image">
                                        <a href="" class="popup_selector" data-inputid="feature_image">Browse Image</a>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"><span data-toggle="tooltip" title="This is Iframe link of Product">Product Video</span><span
                                                class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" value="{{ $product->video_link }}"       name="video_link"
                                               class="form-control col-md-7 col-xs-12" placeholder="< Product Video Iframe Link >">
                                    </div>
                                </div>

                               

                            </div>
                    </form>
                </div>
                <!-- End SmartWizard Content -->
            </div>
        </div>
        @endsection

        @section('pageScript')
            <script type="text/javascript" src="{{ asset('assets/admin/js/ckeditor/ckeditor.js') }}"></script>
            <script src="{{asset('assets/admin/vendors/iCheck/icheck.min.js')}}"></script>
            <script src="{{ asset('assets/admin/js/colorbox/jquery.colorbox-min.js') }}"></script>
            <script type="text/javascript"
                    src="{{asset('packages/barryvdh/elfinder/js/standalonepopup.min.js')}}"></script>
            <script type="text/javascript" src="{{asset('assets/admin/js/common_function.js')}}"></script>
            <!-- Select2 -->
            <script type="text/javascript"
                    src="{{asset('assets/admin/vendors/select2/dist/js/select2.full.min.js')}}"></script>
            <script>
                $(document).ready(function () {
                    $(".select2_single").select2({
                        placeholder: "Select a state",
                        allowClear: true
                    });
                    $(".select2_multiple").select2({
                        maximumSelectionLength: 1,
                        allowClear: true
                    });
                    $('#wizard').smartWizard();

                    $('#wizard_verticle').smartWizard({
                        transitionEffect: 'slide'
                    });

                    $('.buttonNext').addClass('btn btn-success');
                    $('.buttonPrevious').addClass('btn btn-primary');
                    $('.buttonFinish').addClass('btn btn-default');
                });
            </script>


            
@endsection
