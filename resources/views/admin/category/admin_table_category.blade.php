@extends('admin.layouts.master')
@section('pageCss')
         <!-- Datatables -->
<link href="{{asset('assets/admin/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
<link href="{{asset('assets/admin/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css')}}" rel="stylesheet">
<link href="{{asset('assets/admin/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet')}}">
<link href="{{asset('assets/admin/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css')}}" rel="stylesheet">
<link href="{{asset('assets/admin/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css')}}" rel="stylesheet">

<link rel="stylesheet" href="{{ asset('assets/home/css/iziToast.min.css')}}">
    
<script type="text/javascript" src="{{ asset('assets/home/js/iziToast.min.js')}}"></script>
<!-- Datatables -->
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
                @if(Session::has('success'))
                    <script type="text/javascript">
                    iziToast.success({
                        title: 'Success',
                        message: '{{ Session::get('success') }}',
                        position: 'topRight'
                                });
                    </script>
                @endif
                <div class="x_title">
                <h2>Categories <small> List</small></h2>
                <div class="pull-right">
                    <a href="{{route('category.create')}}" class="btn btn-info btn-md">
                        <span class="glyphicon glyphicon-plus"></span> Add
                    </a>
                    <a href="#" class="btn btn-danger btn-md deleteData">
                        <span class="glyphicon glyphicon-trash" ></span> Delete
                    </a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">

              <table id="datatable-checkbox" class="table table-striped table-bordered bulk_action">
                    <thead>
                    <tr>
                        <th><input type="checkbox" id="check-all" class="flat"></th>
                        <th>Category Image</th>
                        <th>Category Name</th>
                        <th>Parent</th>
                        <th>Status</th>
                        <th>Action</th>

                    </tr>
                    </thead>


                    <tbody>
                    @foreach($categories as $c)
                    <tr> 
                        <td><input type="checkbox" class="flat" value="{{$c->id}}" name="table_records"></td>
                        <td>
                            @if($c->image!='')
                                <img src="{{asset($c->image)}}" style="height: 60px;" alt="Product Name Missmatch" />
                            @else
                                <img  src="{{asset('')}}assets/admin/images/noimage.jpg" alt="..." style="height: 60px;">
                            @endif
                        </td>
                        <td>{{$c->name}}</td>
                        <td><?php
                                if ($c->parent_id==NULL) 
                                    echo "Parent";
                                else
                                    echo "Child";
                            ?></td>
                        <td><?php
                                if ($c->status==0) 
                                    echo "Disable";
                                else
                                    echo "Enable";
                            ?>
                        </td>
                        <td>
                           {{--  <a href="{{route('category.show',$c->category_id)}}" data-toggle="tooltip" title="View Detail" class="btn btn-info"><i class="fa fa-eye"></i></a> --}}
                            <a href="{{route('category.edit',$c->id)}}" class="btn btn-default btn-sm"><i class="fa fa-edit"></i> Edit
                            </a>
                        </td>
                    </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@section('pageScript')

<!-- Datatables -->
<script src="{{asset('assets/admin/vendors/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/admin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('assets/admin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('assets/admin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js')}}"></script>
<script src="{{asset('assets/admin/vendors/datatables.net-buttons/js/buttons.flash.min.js')}}"></script>
<script src="{{asset('assets/admin/vendors/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('assets/admin/vendors/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
<script src="{{asset('assets/admin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js')}}"></script>
<script src="{{asset('assets/admin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js')}}"></script>
<script src="{{asset('assets/admin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('assets/admin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js')}}"></script>
<script src="{{asset('assets/admin/vendors/datatables.net-scroller/js/dataTables.scroller.min.js')}}"></script>
<!-- Datatables -->
<!-- iCheck -->
<script src="{{asset('assets/admin/vendors/iCheck/icheck.min.js')}}"></script>

<!-- Datatables -->
    <script>
        $(document).ready(function() {


            $('#datatable').dataTable();

            $('#datatable-keytable').DataTable({
                keys: true
            });

            $('#datatable-responsive').DataTable();

            $('#datatable-fixed-header').DataTable({
                fixedHeader: true
            });

            var $datatable = $('#datatable-checkbox');

            $datatable.dataTable({
                'order': [[ 1, 'asc' ]],
                'columnDefs': [
                    { orderable: false, targets: [0] }
                ]
            });
            $datatable.on('draw.dt', function() {
                $('input').iCheck({
                    checkboxClass: 'icheckbox_flat-green'
                });
            });

        });
    </script>
    <!-- /Datatables -->

    <script type="text/javascript">
        var deleteRow = $('.deleteData');
        var route = "category";
        var token = "{{csrf_token()}}";
        deleteTableRow(deleteRow,route,token);
    </script>
@endsection