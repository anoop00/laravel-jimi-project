@extends('admin.layouts.master')
@section('pageCss')
    <link href="{{ asset('assets/admin/js/colorbox/example2/colorbox.css') }}" rel="stylesheet" type="text/css"/>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Category
                        <small>Add Category</small>
                    </h2>

                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br>
                    @if (count($errors) > 0)
                    <div class="alert alert-danger alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                        </button>
                        <strong>OOPS! You might have missed to fill some required fields. Please check the errors.  <strong>

                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>

                    </div>
                    @endif

                   
                    <form id="demo-form2" data-parsley-validate="" class="form-horizontal form-label-left"
                          action="{{route('category.update',$category->id)}}" method="post">
                        <input type="hidden" name="_method" value="PUT">
                        {{csrf_field()}}
                        

                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Category Name <span class="required">*</span>
                          </label>
                          <div class="col-md-7 col-sm-6 col-xs-12">
                            <input type="text" name="name" value="{{$category->name}}" id="first-name" required="required" class="form-control col-md-7 col-xs-12" placeholder="Category Name">
                            </div>
                        </div>

                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Category Description <span class="required"></span>
                          </label>
                          <div class="col-md-7 col-sm-6 col-xs-12">
                            <textarea id="messageArea" name="description" rows="7" class="form-control ckeditor" placeholder="Write your message..">{{$category->description}}</textarea>
                         </div>
                       </div>

                       <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Parent  <span class="required"></span>
                          </label>
                          {{-- {{ dd($categories) }} --}}
                          <div class="col-md-7 col-sm-6 col-xs-12">
                           <select class="form-control col-md-7 col-xs-12" name="parent_id">
                           <option value="0" >----none-----</option>
                                @foreach($categories as $c)
                                    <option value="{{$c->id}}"  <?php echo ($c->id==$category->parent_id)?'selected':''; ?>>{{$c->name}}</option>
                                @endforeach             
                            </select>
                          </div>
                        </div>

                        


                        <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Image
                          </label>
                          <div class="col-md-7 col-sm-6 col-xs-12">
                            <input type="text" id="feature_image" value="{{$category->image}}" name="image" class="form-control col-md-7 col-xs-12" placeholder="Category Image">
                            <a href="" class="popup_selector" data-inputid="feature_image">Browse Image</a>
                          </div>
                        </div>



                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Status</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select class="form-control" name="status">
                                    <option value="1"<?php echo ($category->status == 1) ? 'selected' : ''; ?>>Enable</option>
                                    <option value="0" <?php echo ($category->status == 0) ? 'selected' : ''; ?>>Disable</option>
                                </select>
                            </div>
                        </div>



                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button type="reset" class="btn btn-primary">Cancel</button>
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('pageScript')
    <script type="text/javascript" src="{{ asset('assets/admin/js/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('assets/admin/js/colorbox/jquery.colorbox-min.js') }}"></script>
    <script type="text/javascript" src="{{asset('packages/barryvdh/elfinder/js/standalonepopup.min.js')}}"></script>
        <!-- iCheck -->
    <script src="{{asset('assets/admin/vendors/iCheck/icheck.min.js')}}"></script>

    @endsection