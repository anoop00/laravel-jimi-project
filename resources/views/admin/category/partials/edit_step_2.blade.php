<div id="step-2">
  <!-- <h2 class="StepTitle">Step 2 Content</h2> -->
  <div class="form-horizontal form-label-left">

   <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Parent <span class="required"></span>
      </label>
      <div class="col-md-7 col-sm-6 col-xs-12">
       <select class="form-control col-md-7 col-xs-12" name="parent_id">
       <option value="0" >----none-----</option>
            @foreach($categories as $c)
                <option value="{{$c->category_id}}"  <?php echo ($c->category_id==$category->parent_id)?'selected':''; ?>>{{$c->name}}</option>
            @endforeach             
        </select>
      </div>
    </div>
      <div class="form-group">
          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Store  <span class="required"></span>
          </label>
          <div class="col-md-7 col-sm-6 col-xs-12">
              <select class="select2_multiple form-control" multiple="multiple" name="store_id[]">
                  @foreach($stores as $s)
                      <option value="{{$s->store_id}}" <?php echo (Helper::in_array_r($s->store_id,$category->store->toArray(),true)) ? 'selected' : '';  ?>>{{$s->store_name}}</option>
                  @endforeach
              </select>
          </div>
      </div>


      <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Top <span class="required"></span>
      </label>
      <div class="col-md-7 col-sm-6 col-xs-12">
        <input type="checkbox" class="flat" name="top" value="{{$category->top}}" <?php echo ($category->top== 1)?'checked':''; ?>>
      </div>
    </div>


   <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Image</span>
      </label>
      <div class="col-md-7 col-sm-6 col-xs-12">
          <input type="text" id="feature_image" value="{{$category->image}}" name="image" class="form-control col-md-7 col-xs-12" placeholder="Category Image">
          <a href="" class="popup_selector" data-inputid="feature_image">Browse Image</a>
      </div>
    </div>


     <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Status  <span class="required"></span>
      </label>
      <div class="col-md-7 col-sm-6 col-xs-12">
        <select class="form-control col-md-7 col-xs-12" name="status">
            <option value="1" <?php echo ($category->status==1)?'selected':''; ?>>Enable</option>
            <option value="0" <?php echo ($category->status==0)?'selected':''; ?>>Disable</option>
        </select>
      </div>
    </div>

    </div>
   </form>
</div>