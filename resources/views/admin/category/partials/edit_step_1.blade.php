<div id="step-1">
      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Category Name <span class="required">*</span>
        </label>
        <div class="col-md-7 col-sm-6 col-xs-12">
          <input type="text" name="name" value="{{$category->name}}" id="first-name" required="required" class="form-control col-md-7 col-xs-12" placeholder="Category Name">
        </div>
      </div>

       <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Category Description <span class="required"></span>
        </label>
        <div class="col-md-7 col-sm-6 col-xs-12">
           <textarea id="messageArea" name="description" rows="7" class="form-control ckeditor" placeholder="Write your message..">{{$category->description}}</textarea>
        </div>
      </div>
     
      

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Meta Tag Title <span class="required">*</span>
        </label>
        <div class="col-md-7 col-sm-6 col-xs-12">
          <input type="text" name="meta_title" value="{{$category->meta_title}}" id="first-name" required="required" class="form-control col-md-7 col-xs-12" placeholder="Meta Tag Title">
        </div>
      </div>


      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Meta Tag Description <span class="required"></span>
        </label>
        <div class="col-md-7 col-sm-6 col-xs-12">
            <textarea rows="4" cols="50" name="meta_description" id="first-name" required="required" class="form-control col-md-7 col-xs-12" placeholder="Meta Tag Description">{{$category->meta_description}}
              </textarea>
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Meta Tag Keyword <span class="required"></span>
        </label>
        <div class="col-md-7 col-sm-6 col-xs-12">
          <input type="text" name="meta_keyword" value="{{$category->meta_keyword}}" id="first-name" required="required" class="form-control col-md-7 col-xs-12" placeholder="Meta Tag Keyword">
        </div>
      </div>

  </div>