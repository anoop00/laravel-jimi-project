<div id="step-2">
      <div class="form-horizontal form-label-left">

       <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Parent  <span class="required"></span>
        </label>
        {{-- {{ dd($categories) }} --}}
        <div class="col-md-7 col-sm-6 col-xs-12">
          <select class="form-control col-md-7 col-xs-12" name="parent_id">
              <option value="0">----none-----</option>
              @foreach($categories as $c)
                {{-- @foreach ($c->children as $children) --}}
                  <option value="{{$c->category_id}}" >{{$c->name}}</option>
                {{-- @endforeach --}}
              @endforeach
          </select>
        </div>
      </div>

      

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Top <span class="required"></span>
        </label>
        <div class="col-md-7 col-sm-6 col-xs-12">
          <input type="checkbox" class="flat" value="1" name="top">
        </div>
      </div>


      <div class="form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Image
        </label>
        <div class="col-md-7 col-sm-6 col-xs-12">
          <input type="text" id="feature_image" value="{{old('image')}}" name="image" class="form-control col-md-7 col-xs-12" placeholder="Category Image">
          <a href="" class="popup_selector" data-inputid="feature_image">Browse Image</a>
        </div>
      </div>



      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Status  <span class="required"></span>
        </label>
        <div class="col-md-7 col-sm-6 col-xs-12">
          <select class="form-control col-md-7 col-xs-12" name="status">
            <option value="1">Enable</option>
            <option value="0">Disable</option>
          </select>
        </div>
      </div>


    </div>
</div>