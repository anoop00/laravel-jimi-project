{{-- <!DOCTYPE html>
<html>
<head>
    <title></title>
    <style type="text/css">
        @import url(https://fonts.googleapis.com/css?family=Roboto:300);

.login-page {
  width: 360px;
  padding: 8% 0 0;
  margin: auto;
}
.form {
  position: relative;
  z-index: 1;
  background: #FFFFFF;
  max-width: 360px;
  margin: 0 auto 100px;
  padding: 45px;
  text-align: center;
  box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);
}
.form input {
  font-family: "Roboto", sans-serif;
  outline: 0;
  background: #f2f2f2;
  width: 100%;
  border: 0;
  margin: 0 0 15px;
  padding: 15px;
  box-sizing: border-box;
  font-size: 14px;
}
.form button {
  font-family: "Roboto", sans-serif;
  text-transform: uppercase;
  outline: 0;
  background: #4CAF50;
  width: 100%;
  border: 0;
  padding: 15px;
  color: #FFFFFF;
  font-size: 14px;
  -webkit-transition: all 0.3 ease;
  transition: all 0.3 ease;
  cursor: pointer;
}
.form button:hover,.form button:active,.form button:focus {
  background: #43A047;
}
.form .message {
  margin: 15px 0 0;
  color: #b3b3b3;
  font-size: 12px;
}
.form .message a {
  color: #4CAF50;
  text-decoration: none;
}
.form .register-form {
  display: none;
}
.container {
  position: relative;
  z-index: 1;
  max-width: 300px;
  margin: 0 auto;
}
.container:before, .container:after {
  content: "";
  display: block;
  clear: both;
}
.container .info {
  margin: 50px auto;
  text-align: center;
}
.container .info h1 {
  margin: 0 0 15px;
  padding: 0;
  font-size: 36px;
  font-weight: 300;
  color: #1a1a1a;
}
.container .info span {
  color: #4d4d4d;
  font-size: 12px;
}
.container .info span a {
  color: #000000;
  text-decoration: none;
}
.container .info span .fa {
  color: #EF3B3A;
}
.form-in-errors {  
            border: 2px solid #e74c3c !important;
            }
body {
  background: #76b852; /* fallback for old browsers */
  background: -webkit-linear-gradient(right, #76b852, #8DC26F);
  background: -moz-linear-gradient(right, #76b852, #8DC26F);
  background: -o-linear-gradient(right, #76b852, #8DC26F);
  background: linear-gradient(to left, #76b852, #8DC26F);
  font-family: "Roboto", sans-serif;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;      
}
    </style>
</head>
<body>
    <div class="login-page">
    <div class="navbar-header">
                    <!-- Branding Image -->
        <a class="navbar-brand" href="{{ url('/') }}">
            {{ config('app.name', 'Laravel') }}
        </a>
    </div>
      <div class="form">
        <form class="form-horizontal" role="form" method="POST" action="{{ route('admin.login.submit') }}">
                        {{ csrf_field() }}

            <input id="email" type="email" class="form-control{{($errors->first('email') ? " form-in-errors" : "")}}" placeholder="E-mail"  name="email" value="{{ old('email') }}" required autofocus>
            <input id="password" type="password"  class="form-control{{($errors->first('password') ? " form-in-errors" : "")}}" name="password"  placeholder="Password" required>
            <button type="submit" class="btn btn-primary">
                Login
            </button>
            <p class="message"><input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me</p>
          <p class="message"><a href="{{ route('admin.password.request') }}">Forget Your Password ??</a></p>
        </form>
      </div>
    </div>
    <script type="text/javascript">
        $('.message a').click(function(){
           $('form').animate({height: "toggle", opacity: "toggle"}, "slow");
        });
    </script>
    {{-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> --}}
{{-- </body>
</html> --}} 


@extends('login.layouts.master')
@section('content')
    <div>
        <a class="hiddenanchor" id="signup"></a>
        <a class="hiddenanchor" id="signin"></a>

        <div class="login_wrapper">
            <div class="animate form login_form">
                <section class="login_content">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('admin.login.submit') }}">
                        {{ csrf_field() }}
                        <h1>Admin Login</h1>
                        @if (count($errors) > 0)
                            <div class="alert alert-danger alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                                </button>
                                <strong>OOPS! You might have missed something. Please check the errors.  <strong>

                                <ul>
                                    @foreach ($errors->all() as $error)
                                       <li>{{ $error }}</li>
                                    @endforeach
                                </ul>

                            </div>
                        @endif
                        <div>
                            <input id="email" type="email" class="form-control{{($errors->first('email') ? " form-in-errors" : "")}}" placeholder="E-mail"  name="email" value="{{ old('email') }}" required autofocus>
                           {{--  <input type="email" class="form-control" name="email" placeholder="Email" value="{{old('email')}}"  /> --}}
                        </div>
                        <div>
                            <input id="password" type="password"  class="form-control{{($errors->first('password') ? " form-in-errors" : "")}}" name="password"  placeholder="Password" required>
                            {{-- <input type="password" class="form-control" name="password" placeholder="Password"  /> --}}
                        </div>
                        <div>
                            <input type="submit" value="login">
                            <a class="reset_pass" href="#">Lost your password?</a>
                        </div>

                        <div class="clearfix"></div>

                        <div class="separator">
                            <div>
                               {{--  <h1><i class="fa fa-paw"></i> Suvalaav</h1>
                                <p>©2016 All Rights Reserved. Suvalaav is an e-commerce platfrom. </p> --}}
                            </div>
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </div>
@endsection